unit WorldDataCode;

// point in polygon functions: http://geomalgorithms.com/a03-_inclusion.html

interface

uses
  imb4,
  Vcl.graphics,
  Vcl.Imaging.pngimage,
  IdCoderMIME, // base64
  System.SysConst,
  System.Math, System.RTTI, System.TypInfo, System.SysUtils,
  System.Classes,
  System.JSON,
  System.Generics.Collections;

type
  TWDKey = UInt32;

const
  wdatNoObjectID =                     TWDKey((icehNoObjectID shl 3) or Ord(wtLengthDelimited)); // TWDID
  wdatObjectID =                       TWDKey((icehObjectID shl 3) or Ord(wtLengthDelimited)); // TWDID, external object id
    wdatNoAttribute =                  TWDKey((icehNoAttribute shl 3) or Ord(wtVarInt)); // uint32, key

// icehWorldCommandBase
  wdatObjectsInquire =                 TWDKey(((icehWorldCommandBase+0) shl 3) or Ord(wtLengthDelimited)); // string, filter
    wdatReturnEventName =              TWDKey(((icehWorldCommandBase+1) shl 3) or Ord(wtLengthDelimited)); // string
    wdatTimeStamp =                    TWDKey(((icehWorldCommandBase+2) shl 3) or Ord(wt64Bit)); // double
    wdatTimeStampLower =               TWDKey(((icehWorldCommandBase+3) shl 3) or Ord(wt64Bit)); // double
    wdatTimeStampUpper =               TWDKey(((icehWorldCommandBase+4) shl 3) or Ord(wt64Bit)); // double
    //todo: same identifiers!?

  wdatSchemaInquire =                  TWDKey(((icehWorldCommandBase+2) shl 3) or wtLengthDelimited); // string, return event name
    wdatSchemaSource =                 TWDKey(((icehWorldCommandBase+3)  shl 3) or wtLengthDelimited); // string
    wdatSchemaCollection =             TWDKey(((icehWorldCommandBase+4) shl 3) or wtLengthDelimited); // string, name of schema ie collection
    wdatSchemaAttributes =             TWDKey(((icehWorldCommandBase+5)  shl 3) or wtLengthDelimited); // blob of pb tagged values

  wdatSchemaAttributeName =            TWDKey(((icehWorldCommandBase+6) shl 3) or wtLengthDelimited); // string
    //****wdatSchemaAttributeWorldType =     TWDKey(((icehWorldCommandBase+7) shl 3) or wtVarInt); // int32
    wdatSchemaAttributeFunction =      TWDKey(((icehWorldCommandBase+8) shl 3) or wtVarInt); // uint32
    wdatSchemaAttributeKey =           TWDKey(((icehWorldCommandBase+9) shl 3) or wtVarInt); // uint32, key, trigger to check schema entry

  //wdatSchemaAttributeAdd =             TWDKey(((icehWorldCommandBase+10) shl 3) or wtVarInt); // uint32, key, trigger to add entry to schema
    // prefixed by:
    //   wdatSchemaCollection
    //   wdatSchemaAttributeName
    //   wdatSchemaAttributeWorldType

  wdatDataSourcesInquire =             TWDKey(((icehWorldCommandBase+11) shl 3) or wtLengthDelimited); // string, return event name

  wdatDataSource =                     TWDKey(((icehWorldCommandBase+12) shl 3) or wtLengthDelimited); // string, private event name of data source
  wdatNoDataSource =                   TWDKey(((icehWorldCommandBase+13) shl 3) or wtLengthDelimited); // string, private event name of data source

  wdatBinsInquire =                    TWDKey(((icehWorldCommandBase+14) shl 3) or wtLengthDelimited); // string, return event name

    wdatBinDescription =               TWDKey(((icehWorldCommandBase+15) shl 3) or wtLengthDelimited); // string
    wdatBinParent =                    TWDKey(((icehWorldCommandBase+16) shl 3) or wtLengthDelimited); // TWDID
    wdatBinRef =                       TWDKey(((icehWorldCommandBase+17) shl 3) or wtLengthDelimited); // TWDID
    wdatBinState =                     TWDKey(((icehWorldCommandBase+18) shl 3) or wtVarInt); // uint32

  wdatBinLoad =                        TWDKey(((icehWorldCommandBase+19) shl 3) or wtLengthDelimited); // TWDID
  wdatBinLoaded =                      TWDKey(((icehWorldCommandBase+20) shl 3) or wtLengthDelimited); // TWDID
  wdatBinUnLoad =                      TWDKey(((icehWorldCommandBase+21) shl 3) or wtLengthDelimited); // TWDID
  wdatBinUnLoaded =                    TWDKey(((icehWorldCommandBase+22) shl 3) or wtLengthDelimited); // TWDID

  wdatBinCreate =                      TWDKey(((icehWorldCommandBase+23) shl 3) or wtLengthDelimited); // TWDID
  wdatBinCreated =                     TWDKey(((icehWorldCommandBase+24) shl 3) or wtLengthDelimited); // TWDID

  wdatBinCopy =                        TWDKey(((icehWorldCommandBase+25) shl 3) or wtLengthDelimited); // TWDID
    wdatBinSourceID =                  TWDKey(((icehWorldCommandBase+26) shl 3) or wtLengthDelimited); // TWDID

  wdatBinDelete =                      TWDKey(((icehWorldCommandBase+27) shl 3) or wtLengthDelimited); // TWDID
  wdatBinDeleted =                     TWDKey(((icehWorldCommandBase+28) shl 3) or wtLengthDelimited); // TWDID

  wdatCollectionAdd =                  TWDKey(((icehWorldCommandBase+29) shl 3) or wtLengthDelimited); // string, name
  wdatCollectionTruncate =             TWDKey(((icehWorldCommandBase+30) shl 3) or wtLengthDelimited); // string, name

  icehObjectTS =                       icehWorldCommandBase+31; // alternative to icehObjectID but then in an ordered list with double (time stamp) as id
  icehNoObjectTS =                     icehWorldCommandBase+32; // alternative to icehNoObjectID, see icehObjectTS above

  wdatObjectTS =                       TWDKey(((icehObjectTS) shl 3) or wt64Bit); // double, time stamp
  wdatNoObjectTS =                     TWDKey(((icehNoObjectTS) shl 3) or wt64Bit); // double, time stamp

  // palette
  icehDiscretePalette =                icehWorldCommandBase+51; // wtLengthDelimited, discrete palette
  icehRampPalette =                    icehWorldCommandBase+52; // wtLengthDelimited, ramp palette


{****
type
  TWDWorldType = Integer;

const
  // TWDWorldType
  wdkGeometry = 1;
  wdkGeometryPart = 2;
  wdkGeometryPoint = 3;
  wdkGridRow = 4;
}
  wdkPaletteDiscrete = 5;
  wdkPaletteRamp = 6;

// geometry types
  gtPoint = 'Point';
  gtLineString = 'LineString';
  gtMultiPoint = 'MultiPoint';
  gtPolygon = 'Polygon';
  gtMultiLineString = 'MultiLineString';

  dotFormat: TFormatSettings = (DecimalSeparator:'.');
  isoDateTimeFormat = 'yyyy-mm-dd hh:nn:ss';
  isoDateTimeFormatSettings: TFormatSettings = (
    DateSeparator:'-';
    TimeSeparator:':';
    ShortDateFormat:'yyyy-mm-dd';
    ShortTimeFormat:'hh:nn:ss'
  );

  // alpha rgb colors
  acBlack =       $FF000000; // default color in most cases to make sure you see something
  acRed =         $FFFF0000;
  acGreen =       $FF00FF00;
  acBlue =        $FF0000FF;
  acWhite =       $FFFFFFFF;
  acTransparent = $00000000;


type
  TWDValue = TByteBuffer;

  TWDEncodedAttributes = TDictionary<TWDKey, TWDValue>;

  TWDID = TWDValue; // id used externally, mostly contains a guid for uniqueness;

  TWDBinState = (
    wbsClosed,
    wbsOpen
  );

  TWDBinStateHelper = record helper for TWDBinState
    function ToString: string;
  end;

  TWDBinLoadState = (
    wblsUnloaded,
    wblsLoading,
    wblsLoaded
  );

  TWDBinLoadStateHelper = record helper for TWDBinLoadState
    function ToString: string;
  end;

  TWDClass = class
  //class function CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; virtual; abstract;
  //****class function WorldType: TWDWorldType; virtual; abstract;
  public
    function Encode: TWDValue; virtual; abstract;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; virtual; abstract;
  end;

  TWDGeometryBase = class(TWDClass)
  private
    function getJSON2D(const aType: string): string; virtual; abstract;
    function getJSONLatLon(const aType: string): string; virtual; abstract;
    function getJSON2DSide(const aSide: Integer; const aFactor: Double): string; virtual; abstract;
  public
    property JSON2D[const aType: string]: string read getJSON2D;
    property JSONLatLon[const aType: string]: string read getJSONLatLon;
    property JSON2DSide[const aSide: Integer; const aFactor: Double]: string read getJSON2DSide;
    procedure translate(dx,dy: Double); virtual;
    function Copy: TWDGeometryBase; virtual; abstract;
  end;

  TWDGeometryPoint = class(TWDGeometryBase)
  constructor Create; overload;
  constructor Create(ax, ay, az: Double); overload;
  constructor CreateFromSVGPath(const aSVG: string);
  //class function CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  //****class function WorldType: TWDWorldType; override;
  private
    function getJSON2D(const aType: string): string; override;
    function getJSONLatLon(const aType: string): string; override;
    function getJSON2DSide(const aSide: Integer; const aFactor: Double): string; override;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
    function Copy: TWDGeometryBase; override;
  public
    x: Double;
    y: Double;
    z: Double;
    procedure translate(dx, dy: Double); override;
  end;

  // specific low overhead structures for geometric calculations

  TWDPoint2D = record
  class function Create(aX, aY: Double): TWDPoint2D; overload; static;
  class function Create(aPoint: TWDGeometryPoint): TWDPoint2D; overload; static;
  public
    x: Double;
    y: Double;
    class function ccw(const A, B, C: TWDPoint2D): Double; static; inline;
    class function intersect(const A, B, C, D:TWDPoint2D): Boolean; static;
  end;

  TWDLine2D = record
  class function Create(aX1, aY1, aX2, aY2: Double): TWDLine2D; overload; static;
  class function Create(aP1, aP2: TWDGeometryPoint): TWDLine2D; overload; static;
  public
    p1: TWDPoint2D;
    p2: TWDPoint2D;
    class function intersect(const L1, L2: TWDLine2D): Boolean; static;
  end;


  {
  TWDGeometryExtent  = class(TWDClass)
  constructor Create; overload;
  constructor Create(axMin,ayMin,azMin, axMax,ayMax,azMax: Double); overload;
  class function WorldType: TWDWorldType; override;
  public
    function Encode: TWDValue; override;
    procedure Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer); override;
  public
    // todo: 2 points or specific coordinates
    xMin: Double;
    yMin: Double;
    zMin: Double;
    xMax: Double;
    yMax: Double;
    zMax: Double;
  end;
  }

  TWDGeometryPart = class(TWDClass)
  constructor Create;
  destructor Destroy; override;
  //class function CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  //****class function WorldType: TWDWorldType; override;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  private
    fPoints: TObjectList<TWDGeometryPoint>;
    function getJSON2D: string;
    function getJSONLatLon: string;
    function getJSON2DSide(const aSide: Integer; const aFactor: Double): string;
  public
    property points: TObjectList<TWDGeometryPoint> read fPoints;
    property JSON2D: string read getJSON2D;
    property JSONLatLon: string read getJSONLatLon;
    property JSON2DSide[const aSide: Integer; const aFactor: Double]: string read getJSON2DSide;
    procedure AddPoint(x, y, z: Double);
    // point in polygon functions
    function cn_PnPoly(x, y: Double): Boolean;
    function wn_PnPoly(x, y: Double): Integer;
    procedure translate(dx,dy: Double);
  end;

  TWDGeometry = class(TWDGeometryBase)
  constructor Create;
  constructor CreateFromSVGPath(const aSVGPath: string);
  destructor Destroy; override;
  //class function CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  //****class function WorldType: TWDWorldType; override;
  private
    function getJSON2D(const aType: string): string; override;
    function getJSONLatLon(const aType: string): string; override;
    function getJSON2DSide(const aSide: Integer; const aFactor: Double): string; override;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  private
    fParts: TObjectList<TWDGeometryPart>;
  public
    property parts: TObjectList<TWDGeometryPart> read fParts;

    function AddPart: TWDGeometryPart;
    procedure AddPoint(x, y, z: Double); overload;
    procedure AddPoint(aPoint: TWDGeometryPoint); overload; // ref only!

    function PointInFirstPart(x, y: Double): Boolean;
    function PointInAnyPart(x, y: Double): Boolean;
    function PointsInAnyPart(aGeometry: TWDGeometry): Boolean;

    function intersectsSortOf(aGeometry: TWDGeometry): Boolean; overload;
    function intersectsSortOf(const aLine: TWDLine2D): Boolean; overload;

    procedure translate(dx, dy: Double); override;
    function Copy: TWDGeometryBase; override;
  end;

  TWDExtent = record
  class function Create: TWDExtent; overload; static;
  class function Create(aXMin, aYMin, aXMax, aYMax: Double): TWDExtent; overload; static;
  class function FromGeometry(aGeometry: TWDGeometry): TWDExtent; static;
  public
    procedure Init; overload;
    procedure Init(x, y: Double); overload;
    procedure Init(x, y, aWidth, aHeight: Double); overload;
  public
    xMin: Double;
    yMin: Double;
    xMax: Double;
    yMax: Double;
    function IsEmpty: Boolean;
    function Expand(x, y: Double): Boolean; overload;
    function Expand(aExtent: TWDExtent): Boolean; overload;
    function Expand(aGeometry: TWDGeometry): Boolean; overload;
    function Intersects(const aExtent: TWDExtent): Boolean;
    function Contains(x, y: Double): Boolean;
    function CenterX: Double;
    function CenterY: Double;
    //function Square: TWDExtent;
    procedure translate(dx, dy: Double);
  public
    function Encode: TWDValue;
    procedure Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
  end;

  TAlphaRGBPixel = Cardinal; // 32 bit ARGB color

  TGeoColors = record
  class function Create(aFillColor: TAlphaRGBPixel=acBlack; aOutlineColor: TAlphaRGBPixel=acTransparent; aFill2Color: TAlphaRGBPixel=acTransparent): TGeoColors; overload; static;
  class function Create(aJSON: TJSONObject; aDefault: TAlphaRGBPixel=acTransparent): TGeoColors; overload; static;
  class function Create(aJSON: TJSONValue; const aPath: string=''; aDefault: TAlphaRGBPixel=acTransparent): TGeoColors; overload; static;
  public
    fillColor: TAlphaRGBPixel;
    outlineColor: TAlphaRGBPixel;
    fill2Color: TAlphaRGBPixel; // for pattern
    function mainColor(aDefaultColor: TAlphaRGBPixel=acTransparent): TAlphaRGBPixel;
    function toJSON: string;
  end;

  TWDPalette = class(TWDClass)
  constructor Create(const aDescription: string); overload;
  function Clone: TWDPalette;
  class function wdTag: UInt32; virtual; abstract;
  public
    function Encode: TWDValue; override;
    function Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass; override;
  protected
    fDescription: string;
    function _clone: TWDPalette; virtual; abstract;
  public
    function ValueToColors(const aValue: Double): TGeoColors; virtual; abstract;
    property description: string read fDescription;
    function minValue: Double; virtual; abstract;
    function maxValue: Double; virtual; abstract;
  end;

const
  // TWDFunction, world function value
  wdfConsumerOptional = $01;
  wdfConsumer = $02;
  wdfProducerOptional = $04;
  wdfProducer = $08;
  wdfStored = $40;

  emptyExtent: TWDExtent = (xMin: NaN);

type
  TWDFunction = UInt32; // single function, see wdf.. above
  TWDFunctions = UInt32; // or-ed together TWDFunction values

  // RTTI (de)coding custom
  TWorldKey = class(TCustomAttribute)
  constructor Create(aKey: TWDKey);
  constructor CreateWithWiretype(aTag: TWDKey; aWireType: TWDKey);
  private
    fKey: TWDKey;
  public
    property wKey: TWDKey read fKey;
  end;

  TWorldFunctions = class(TCustomAttribute)
  constructor Create(aFunctions: TWDFunctions);
  private
    sFunctions: TWDFunctions;
  public
    property wFunctions: TWDFunctions read sFunctions;
  end;
  {****
  TWorldType = class(TCustomAttribute)
  constructor Create(aWorldType: TWDWorldType);
  private
    fType: TWDWorldType;
  public
    property wType: TWDWorldType read fType;
  end;
  }
  TWorldName = class(TCustomAttribute)
  constructor Create(const aWorldName: string);
  private
    fName: string;
  public
    property wName: string read fName;
  end;

  TWDClassCacheField = class
  constructor Create(aRttiField: TRttiField; const aName: string; aKey: TWDKey; aFunctions: TWDFunctions; {****aType: TWDWorldType; }const aSource: TWDID);
  destructor Destroy; override;
  private
    fRttiField: TRttiField;
    fName: string;
    fKey: TWDKey;
    fFunctions: TWDFunctions;
    //****fType: TWDWorldType;
    fDominantSource: TWDID;
  public
    property RttiField: TRttiField read fRttiField;
    property Name: string read fName;
    property Key: TWDKey read fKey write fKey;
    property Functions: TWDFunctions read fFunctions;
    //****property WType: TWDWorldType read fType;
    property DominantSource: TWDID read fDominantSource;

    function Update(aKey: TWDKey; aFunctions: TWDFunctions; const aSource: TWDID): Boolean;
  end;

  TWDClassCache = class
  //constructor Create(aObject: TObject; const aSource: TWDID); overload;
  constructor Create(aClass: TClass; const aSource: TWDID); overload;
  destructor Destroy; override;
  private
    fRttiContext: TRttiContext;
    fClass: TClass;
    fAttributesOnName: TObjectDictionary<string, TWDClassCacheField>;
    fAttributesOnKey: TObjectDictionary<TWDKey, TWDClassCacheField>;
  public
    function NextFreeKey(aKey: TWDKey): TWDKey;
  public
    class function getAttributeWireType(aRttiField: TRttiField): TWDKey;

    function getAttribute(aObject: TObject; aKey: TWDKey; aRttiField: TRttiField): TWDValue;
    function EncodeAttributes(aObject: TObject): TWDValue; overload;
    procedure EncodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes); overload;
    function EncodeAttributesWithoutObjectID(aObject: TObject): TWDValue;
    function EncodeAttributesFiltered(aObject: TObject; aAttributes: TArray<TWDKey>): TWDValue;
    function setAttribute(aObject: TObject; aKey: TWDKey; const aValue: TWDValue; var aCursor: Integer): Boolean;
    procedure DecodeAttributes(aObject: TObject; const aEncodedAttributes: TWDValue); overload;
    procedure DecodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes); overload;

    property attributesOnName: TObjectDictionary<string, TWDClassCacheField> read fAttributesOnName;
    property attributesOnKey: TObjectDictionary<TWDKey, TWDClassCacheField> read fAttributesOnKey;
  end;


function CompareHighestFunction(aFunctions1, aFunctions2: TWDFunctions): Integer;

function NewGUIDAsWDID: TWDID;


function DoubleToJSON(d: Double): string;
function ColorToJSON(aColor: TAlphaRGBPixel): string;

function RGBToAlphaColor(aRed, aGreen, aBlue: Byte; aAlpha: Byte=$FF): TAlphaRGBPixel; overload;
function RGBToAlphaColor(aRed, aGreen, aBlue: Byte; aAlpha: Double): TAlphaRGBPixel; overload; // aAlpha: 0.0..1.0
function StringToAlphaColor(const s: string): TAlphaRGBPixel; // assumes 'rrbbggaa' chars are 0..f
function JSONToAlphaColor(aJSONValue: TJSONValue; aDefault: TAlphaRGBPixel=acBlack): TAlphaRGBPixel; overload;
function JSONToAlphaColor(aJSONObject: TJSONObject; const aPath: string; aDefault: TAlphaRGBPixel=acBlack): TAlphaRGBPixel; overload;

function APOpacity(aColor: TAlphaRGBPixel): Double;
function OpacityToJSON(aColor: TAlphaRGBPixel): string;

// images and base64
function ImageToBytes(aImage: TPngImage): TBytes;
function GraphicToBytes(aGraphic: TGraphic): TBytes;
procedure BytesToImage(const aBytes: TBytes; aImage: TPngImage);
function ImageToBase64(aImage: TPngImage): string;
function PNGFileToBase64(const aFileName: string): string;


implementation


function CompareHighestFunction(aFunctions1, aFunctions2: TWDFunctions): Integer;
// >0: aFunctions1 > aFunctions2
// =0: aFunctions1 = aFunctions2
// <0: aFunctions1 < aFunctions2
begin
  // strip to highest bit
  while (aFunctions1<>0) and (aFunctions2<>0) do
  begin
    aFunctions1 := aFunctions1 shr 1;
    aFunctions2 := aFunctions2 shr 1;
  end;
  Result := aFunctions1-aFunctions2;
end;

function NewGUIDAsWDID: TWDID;
var
  g: TGUID;
begin
  g := TGUID.NewGuid;
  SetLength(Result, SizeOf(g));
  Move(g, PAnsiChar(Result)^, SizeOf(g));
end;

function DoubleToJSON(d: Double): string;
begin
  if d.IsNan
  then Result := 'null'
  else Result := d.toString(dotFormat);
end;

function ColorToJSON(aColor: TAlphaRGBPixel): string;
begin
  Result := '#'+(aColor and $FFFFFF).ToHexString(6);
end;

(*
function JSONToColor(const aJSON: string): TAlphaRGBPixel;
begin
  if aJSON<>'' then
  begin
    if aJSON[1]='#' then
    begin
      // hex format, 6 chars -> #RRGGBB, 3 chars #RGB -> #RRGGBB,  8 chars #ARGB
    end
    else if aJSON[1] in ['A'..'Z','a'..'z'] then
    begin
      // color name
    end
    else if aJSON[1] in ['0'..'9'] then
    begin
      // decimal number
    end
    else if aJSON[1]='{' then
    begin
      // structure {"R":r, "G":g, "B":b, "A":a}, default 255 for all values
    end
    else if aJSON[1]='[' then
    begin

    end;
  end;

end;
*)

function RGBToAlphaColor(aRed, aGreen, aBlue, aAlpha: Byte): TAlphaRGBPixel;
begin
  Result := (aAlpha shl 24) or (aRed shl 16) or (aGreen shl 8) or aBlue;
end;

function RGBToAlphaColor(aRed, aGreen, aBlue: Byte; aAlpha: Double): TAlphaRGBPixel;
// aAlpha: 0.0..1.0
begin
  Result := RGBToAlphaColor(aRed, aGreen, aBlue, byte(round(max(min(aAlpha, 1.0), 0.0)*255)));
end;

{
function HexCharToInt(aHexChar: Char): Integer; inline;
begin
  case aHexChar of
    '0'..'9': Result := Ord(aHexChar)-Ord('0');
    'a'..'f': Result := 10+Ord(aHexChar)-Ord('a');
    'A'..'F': Result := 10+Ord(aHexChar)-Ord('A');
  else
              Result := -1;
  end;
end;
}

function StringToAlphaColor(const s: string): TAlphaRGBPixel;
// assumes 'rrbbggaa' chars are 0..f
begin
  Result := RGBToAlphaColor(
    Integer.Parse('$'+s.Substring(0, 2)),
    Integer.Parse('$'+s.Substring(2, 2)),
    Integer.Parse('$'+s.Substring(4, 2)),
    Integer.Parse('$'+s.Substring(6, 2)));
end;

function JSONToAlphaColor(aJSONValue: TJSONValue; aDefault: TAlphaRGBPixel): TAlphaRGBPixel;
var
  alpha: TAlphaRGBPixel;
  jsonArray: TJSONArray;
  s: string;
begin
  // [r,g,b,a]
  if aJSONValue is TJSONArray then
  begin
    jsonArray := aJSONValue as TJSONArray;
    if jsonArray.Count>2 then
    begin
      if jsonArray.Count>3
      then alpha := Round(Max(Min((jsonArray.Items[3] as TJSONNumber).AsDouble, 1.0), 0.0)*255)
      else
      begin
        if aDefault<>acTransparent
        then alpha := (aDefault shr 24) and $FF
        else alpha := $FF;
      end;
      Result := RGBToAlphaColor(
        (jsonArray.Items[0] as TJSONNumber).AsInt,
        (jsonArray.Items[1] as TJSONNumber).AsInt,
        (jsonArray.Items[2] as TJSONNumber).AsInt,
        alpha);
    end
    else Result := aDefault;
  end
  else
  begin
    //aJSONValue.ToString
    // todo: decode string version of color (with or without alpha color)
    s := aJSONValue.value.ToLower.Trim;
    if s.StartsWith('#')
    then s := s.Substring(1);
    if s.StartsWith('0x')
    then s := s.Substring(3);
    case s.Length of
      3: Result := StringToAlphaColor(s[1]+s[1]+s[2]+s[2]+s[3]+s[3]+'ff');
      4: Result := StringToAlphaColor(s[1]+s[1]+s[2]+s[2]+s[3]+s[3]+s[4]+s[4]);
      6: Result := StringToAlphaColor(s+'ff');
      8: Result := StringToAlphaColor(s);
    else
      Result := aDefault;
    end;
  end;
end;

function JSONToAlphaColor(aJSONObject: TJSONObject; const aPath: string; aDefault: TAlphaRGBPixel): TAlphaRGBPixel;
var
  jsonValue: TJSONValue;
begin
  if aJSONObject.TryGetValue<TJSONValue>(aPath, jsonValue)
  then Result := JSONToAlphaColor(jsonValue, aDefault)
  else Result := aDefault;
end;

function APOpacity(aColor: TAlphaRGBPixel): Double;
begin
  Result := (aColor shr 24)/255;
end;

function OpacityToJSON(aColor:  TAlphaRGBPixel): string;
var
  opacity: Double;
begin
  opacity := (aColor shr 24)/255;
  Result := opacity.ToString(TFloatFormat.ffFixed, 3, 2, dotFormat)
end;

{ utils }

function ImageToBytes(aImage: TPngImage): TBytes;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create;
  try
    aImage.SaveToStream(stream);
  	Result := stream.Bytes;
  finally
    stream.Free;
  end;
end;

function GraphicToBytes(aGraphic: TGraphic): TBytes;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create;
  try
    aGraphic.SaveToStream(stream);
  	Result := stream.Bytes;
  finally
    stream.Free;
  end;
end;

{
function CreateImageFromBytes(const aBytes: TBytes): TPngImage;
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create(aBytes);
  try
    Result := TPngImage.Create;
    Result.LoadFromStream(stream);
  finally
    stream.Free;
  end;
end;
}
procedure BytesToImage(const aBytes: TBytes; aImage: TPngImage);
var
  stream: TBytesStream;
begin
  stream := TBytesStream.Create(aBytes);
  try
    aImage.LoadFromStream(stream);
  finally
    stream.Free;
  end;
end;

function ImageToBase64(aImage: TPngImage): string;
var
  ms: TMemoryStream;
begin
  if Assigned(aImage) then
  begin
    ms := TMemoryStream.Create;
    try
      aImage.SaveToStream(ms);
      ms.Position := 0;
      Result := 'data:image/png;base64,'+TIdEncoderMIME.EncodeStream(ms);
    finally
      ms.Free;
    end;
  end
  else Result := '';
end;

function PNGFileToBase64(const aFileName: string): string;
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    fs.Position := 0;
    Result := 'data:image/png;base64,'+TIdEncoderMIME.EncodeStream(fs);
  finally
    fs.Free;
  end;
end;

{ TWDBinStateHelper }

function TWDBinStateHelper.ToString: string;
begin
  case Self of
    wbsClosed:
      Result := 'open';
    wbsOpen:
      Result := 'open';
  else
    Result := '## unknown';
  end;
end;

{ TWDBinLoadStateHelper }

function TWDBinLoadStateHelper.ToString: string;
begin
  case Self of
    wblsUnloaded:
      Result := 'unloaded';
    wblsLoading:
      Result := 'loading';
    wblsLoaded:
      Result := 'loaded';
  else
    Result := '## unknown';
  end;
end;


{ TWorldKey }

// http://docwiki.embarcadero.com/RADStudio/XE5/en/Extracting_Attributes_at_Run_Time

constructor TWorldKey.Create(aKey: TWDKey);
begin
  inherited Create;
  fKey := aKey;
end;

constructor TWorldKey.CreateWithWiretype(aTag, aWireType: TWDKey);
begin
  Create((aTag shl 3) or aWireType);
end;

{ TWorldFunction }

constructor TWorldFunctions.Create(aFunctions: TWDFunctions);
begin
  inherited Create;
  sFunctions := aFunctions;
end;

{ TWorldType }
{****
constructor TWorldType.Create(aWorldType: TWDWorldType);
begin
  inherited Create;
  fType := aWorldType;
end;
}
{ TWorldName }

constructor TWorldName.Create(const aWorldName: string);
begin
  inherited Create;
  fName := aWorldName;
end;

{ TWDClassCacheField }

constructor TWDClassCacheField.Create(aRttiField: TRttiField; const aName: string; aKey: TWDKey; aFunctions: TWDFunctions; {****aType: TWDWorldType; }const aSource: TWDID);
begin
  inherited Create;
  fRttiField := aRttiField; // ref only
  fName := aName;
  fKey := aKey;
  fFunctions := aFunctions;
  //****fType := aType;
  fDominantSource := aSource;
end;

destructor TWDClassCacheField.Destroy;
begin
  fRttiField := nil; // remove ref
  inherited;
end;

function TWDClassCacheField.Update(aKey: TWDKey; aFunctions: TWDFunctions; const aSource: TWDID): Boolean;
begin
  Result := False; // sentinel
  // check key for change
  if fKey<>aKey then
  begin
    fKey := aKey;
    Result := True;
  end;
  // check change in functions and for more dominant function
  if (fFunctions or aFunctions) <> fFunctions then
  begin
    if (aFunctions>fFunctions) and (aSource<>'')
    then fDominantSource := aSource;
    fFunctions := fFunctions or aFunctions;
    Result := True;
  end;
end;

{ TWDClassCacheEntry }

constructor TWDClassCache.Create(aClass: TClass; const aSource: TWDID);
var
  tt: TRttiType;
  ti: PTypeInfo;
  fields: TArray<TRttiField>;
  f: TRttiField;
  attributes: TArray<TCustomAttribute>;
  a: TCustomAttribute;
  wKey, nextKey: TWDKey;
  wFunctions: UInt32;
  //****wType: TWDWorldType;
  wName: string;
  ccf: TWDClassCacheField;
begin
  inherited Create;
  fClass := aClass;
  fAttributesOnName := TObjectDictionary<string, TWDClassCacheField>.Create([doOwnsValues]);
  fAttributesOnKey := TObjectDictionary<TWDKey, TWDClassCacheField>.Create([]);
  fRttiContext := TRttiContext.Create;
  // find attributes via RTTI with TProtoBufKey defined
  nextKey := icehAttributeBase shl 3;
  ti := aClass.ClassInfo;
  tt := fRttiContext.GetType(ti); // todo: if we re-create the RTTI context every time do we lose the link to the stored fields?
  fields := tt.GetFields;
  for f in fields do
  begin
    // defaults
    wKey := 0;
    wFunctions := 0;
    //****wType := -1;
    wName := '';
    // decode custom attributes
    attributes := f.GetAttributes;
    for a in attributes do
    begin
      if a.ClassType=TWorldKey
      then wKey := (a as TWorldKey).wKey
      else if a.ClassType=TWorldFunctions
      then wFunctions := wFunctions or (a as TWorldFunctions).wFunctions
      {****
      else if a.ClassType=TWorldType
      then wType := (a as TWorldType).wType
      }
      else if a.ClassType=TWorldName
      then wName := (a as TWorldName).wName;
    end;
    // detect type
    {****
    if wType=-1 then
    begin
      if (f.FieldType.TypeKind=tkClass) and f.FieldType.ClassType.InheritsFrom(TWDClass)
      then wType := TWDClass(f.FieldType.ClassType).WorldType
      else wType := 0;
      // .. extend non-classes here
    end;
    }
    // build key if not defined
    if (wKey=0)
    then wKey := nextKey or TWDClassCache.getAttributeWireType(f);
    // make key unique
    while fAttributesOnKey.ContainsKey(wKey)
    do wKey := wKey+(1 shl 3);
    // default or specific name
    if wName=''
    then wName := f.Name;
    // add attribute
    ccf := TWDClassCacheField.Create(f,  wName, wKey, wFunctions,  {****wType, }aSource);
    fAttributesOnName.AddOrSetValue(ccf.Name, ccf);
    // only add non-consumer attributes on key (they will be linked later by the schema)
    //if wFunctions>=wdfProducerOptional
    //then
    fAttributesOnKey.AddOrSetValue(ccf.Key, ccf);
    nextKey := (wKey and not 7)+(1 shl 3);
  end;
end;

//constructor TWDClassCache.Create(aObject: TObject; const aSource: TWDID);
//begin
//  Create(aObject.ClassType, aSource);
//end;

procedure TWDClassCache.DecodeAttributes(aObject: TObject; const aEncodedAttributes: TWDValue);
var
  cursor: Integer;
  key: TWDKey;
begin
  cursor := 0;
  while cursor<Length(aEncodedAttributes) do
  begin
    key := aEncodedAttributes.bb_read_uint32(cursor);

    if not setAttribute(aObject, key, aEncodedAttributes, cursor)
    then aEncodedAttributes.bb_read_skip(cursor, key and 7);
  end;
end;

procedure TWDClassCache.DecodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes);
var
  eap: TPair<TWDKey, TWDValue>;
  cursor: Integer;
begin
  for eap in aEncodedAttributes do
  begin
    cursor  := 0;
    setAttribute(aObject, eap.Key, eap.Value, cursor);
  end;
end;

destructor TWDClassCache.Destroy;
begin
  FreeAndNil(fAttributesOnKey);
  FreeAndNil(fAttributesOnName);
  inherited;
  fRttiContext.Free;
end;

procedure TWDClassCache.EncodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes);
var
  kf: TPair<TWDKey, TWDClassCacheField>;
begin
  for kf in fAttributesOnKey
  do aEncodedAttributes.AddOrSetValue(kf.Key, getAttribute(aObject, kf.Key, kf.Value.fRttiField));
end;

function TWDClassCache.EncodeAttributesFiltered(aObject: TObject; aAttributes: TArray<TWDKey>): TWDValue;
var
  attrKey: TWDKey;
  attrField: TWDClassCacheField;
begin
  Result := '';
  for attrKey in aAttributes do
  begin
    if fAttributesOnKey.TryGetValue(attrKey, attrField)
    then Result := Result+TByteBuffer.bb_uint32(attrKey)+getAttribute(aObject, attrKey, attrField.RttiField);
  end;
end;

function TWDClassCache.EncodeAttributesWithoutObjectID(aObject: TObject): TWDValue;
var
  kf: TPair<TWDKey, TWDClassCacheField>;
begin
  Result := '';
  for kf in fAttributesOnKey do
  begin
    if kf.Key<>wdatObjectID
    then Result := Result+TByteBuffer.bb_uint32(kf.Key)+getAttribute(aObject, kf.Key, kf.Value.RttiField);
  end;
end;

function TWDClassCache.getAttribute(aObject: TObject; aKey: TWDKey; aRttiField: TRttiField): TWDValue;
var
  value: TValue;
begin
  value := aRttiField.GetValue(aObject);
  case value.Kind of
    //      tkUnknown: ;
    tkInteger:
      begin
        if value.TypeInfo=TypeInfo({Integer}Int32)
        then Result := TByteBuffer.bb_int32(TValueData(value).FAsSLong)
        else if value.TypeInfo=TypeInfo(UInt32)
        then Result := TByteBuffer.bb_uint32(TValueData(value).FAsULong)
        else if value.TypeInfo=TypeInfo(Byte)
        then Result := TByteBuffer.bb_int32(TValueData(value).FAsUByte)
        else if value.TypeInfo=TypeInfo(Word)
        then Result := TByteBuffer.bb_int32(TValueData(value).FAsUWord)
        else raise Exception.Create('Unsupported int type '+string(value.TypeInfo.Name)+' in TWDClassCacheEntry.getAttribute');
      end;
    tkEnumeration:
      Result := TByteBuffer.bb_uint32(TValueData(value).FAsULong);
    tkFloat:
      begin
        if value.TypeInfo=TypeInfo(Double)
        then Result := TByteBuffer.bb_bytes(TValueData(value).FAsDouble, SizeOf(Double))
        else if value.TypeInfo=TypeInfo(Single)
        then Result := TByteBuffer.bb_bytes(TValueData(value).FAsSingle, SizeOf(Single))
        else raise Exception.Create('Unsupported float type ('+string(value.TypeInfo.Name)+') in TWDClassCacheEntry.getAttribute');
      end;
    tkChar, // AnsiChar, 1
    tkWChar:
      Result := TByteBuffer.bb_uint64(value.DataSize)+TByteBuffer.bb_bytes(value.GetReferenceToRawData^, value.DataSize);
    tkString:
      Result := TByteBuffer.bb_ansi_string(PShortString(value.GetReferenceToRawData)^);
//      tkSet: ;
    tkClass:
      begin
        // todo: process supported classes as attribute
        if value.AsObject is TWDClass then
        begin
          Result := (value.AsObject as TWDClass).Encode;
          Result := TByteBuffer.bb_uint64(Length(Result))+Result;
        end
        else raise Exception.Create('Unsupported attribute class ('+value.AsObject.ClassName+') in TWDClassCacheEntry.getAttribute');
      end;
//      tkMethod: ;
    tkLString:
      Result := TByteBuffer.bb_ansi_string(PAnsiString(value.GetReferenceToRawData)^);
    tkWString:
      Result := TByteBuffer.bb_utf8_string(UTF8String(PWideString(value.GetReferenceToRawData)^));
//      tkVariant: ;
//      tkArray:
//        Result := PrepareArrayElements(value, aTypeInfo);
    tkRecord:
      if value.TypeInfo=TypeInfo(TGUID)
      then Result := TByteBuffer.bb_uint64(sizeof(TGUID))+TByteBuffer.bb_bytes(TGUID(value.GetReferenceToRawData^), sizeof(TGUID));
//        else Result := PrepareFields(value.GetReferenceToRawData, aTypeInfo);
//      tkInterface: ;
    tkInt64:
      if value.TypeInfo=TypeInfo(Int64)
      then Result := TByteBuffer.bb_int64(TValueData(value).FAsSInt64)
      else if value.TypeInfo=TypeInfo(UInt64)
      then Result := TByteBuffer.bb_uint64(TValueData(value).FAsUInt64)
      else raise Exception.Create('Unsupported int64 type '+string(value.TypeInfo.Name)+' in TWDClassCacheEntry.getAttribute');
//      tkDynArray:
//        Result := PrepareDynamicArrayElements(value, aTypeInfo);
    tkUString:
      Result := TByteBuffer.bb_string(value.AsString);
//      tkClassRef: ;
//      tkPointer: ;
//      tkProcedure: ;
  else
    raise Exception.Create('Unsupported field type ('+IntToStr(Ord(value.Kind))+') in TWDClassCacheEntry.getAttribute');
  end;
end;

class function TWDClassCache.getAttributeWireType(aRttiField: TRttiField): TWDKey;
begin
  case aRttiField.FieldType.TypeKind of
    //tkUnknown: ;
    tkInteger:        Result := wtVarInt;
    tkChar:           Result := wtLengthDelimited;
    tkEnumeration:    Result := wtVarInt;
    tkFloat:
      begin
        if aRttiField.FieldType.TypeSize=8
        then Result := wt64Bit
        else if aRttiField.FieldType.TypeSize=4
        then Result := wt32Bit
        else raise Exception.Create('Unsupported float type ('+IntToStr(aRttiField.FieldType.TypeSize)+') for '+aRttiField.Name+' in TWDClassCache.getAttributeWireType');
      end;
    tkString:         Result := wtLengthDelimited;
    //tkSet: ;
    tkClass:
      begin
        // todo: why is the type name expanded with generics
        //if aRttiField.FieldType.Name=TWDGeometry.ClassName
        if aRttiField.FieldType.BaseType.Name=TWDClass.ClassName
        then Result := wtLengthDelimited
        else raise Exception.Create('Unsupported class type '+aRttiField.Name+' in TWDClassCache.getAttributeWireType');
      end;
    //tkMethod: ;
    tkWChar:          Result := wtLengthDelimited;
    tkLString:        Result := wtLengthDelimited;
    tkWString:        Result := wtLengthDelimited;
    //tkVariant: ;
    //tkArray: ;
    tkRecord:         Result := wtLengthDelimited;
    //tkInterface: ;
    tkInt64:          Result := wtVarInt;
    //tkDynArray: ;
    tkUString:        Result := wtLengthDelimited;
    //tkClassRef: ;
    //tkPointer: ;
    //tkProcedure: ;
  else
    raise Exception.Create('Unsupported field type ('+IntToStr(Ord(aRttiField.FieldType.TypeKind))+') for '+aRttiField.Name+' in TWDClassCache.getAttributeWireType');
  end;
end;

function TWDClassCache.NextFreeKey(aKey: TWDKey): TWDKey;
var
  keywoWT: TWDKey;
  akp: TPair<string, TWDClassCacheField>;
  akpwoWT: TWDKey;
begin
  // calc given key without wire type
  keywoWT := aKey shr 3;
  // check to min value of key, (-1 because we are going to get the NEXT key)
  if keywoWT<icehAttributeBase-1
  then keywoWT := icehAttributeBase-1;
  // check for existing higher keys
  for akp in fAttributesOnName do
  begin
    akpwoWT := akp.Value.Key shr 3;
    if keywoWT<akpwoWT
    then keywoWT := akpwoWT;
  end;
  // rebuild key with wire type as the NEXT free key
  Result := ((keywoWT+1) shl 3) or (aKey and 7);
end;

{
function TWDClassCache.getIsComplete: Boolean;
var
  nccfp: TPair<string, TWDClassCacheField>;
  classCacheField: TWDClassCacheField;
begin
  for nccfp in fAttributesByName do
  begin
    if (nccfp.Value.Key shr 3)>0 then
    begin
      if AttributesByKey.TryGetValue(nccfp.Value.Key, classCacheField) then
      begin
        if nccfp.Value<>classCacheField then
        begin
          // handle key collision or re-key!
          // for now remove from key list to recheck on next change?
          AttributesByKey.Remove(nccfp.Value.Key);
          Exit(False);
        end;
      end
      else AttributesByKey.Add(nccfp.Value.Key, nccfp.Value);
    end
    else Exit(False);
  end;
  // if all pass then return compete ie true
  Exit(True);
end;
}

function TWDClassCache.setAttribute(aObject: TObject; aKey: TWDKey; const aValue: TWDValue; var aCursor: Integer): Boolean;
var
  len: UInt64;
  field: TWDClassCacheField;
  value: TValue;

  procedure CheckKey(aWireType: Integer);
  begin
    if Integer(aKey and 7)<>aWireType
    then raise Exception.Create('wire type mismatch in type '+string(field.Name)+' in TWDClassCacheEntry.setAttribute: '+aKey.ToString+' <> '+aWireType.ToString);
  end;

begin
  if fAttributesOnKey.TryGetValue(aKey, field) then
  begin
    Result := True;
    value := field.RttiField.GetValue(aObject);
    case value.Kind of
      //      tkUnknown: ;
      tkInteger:
        begin
          CheckKey(wtVarInt);
          if value.TypeInfo=TypeInfo({Integer}Int32)
          then value := aValue.bb_read_int32(aCursor)
          else if value.TypeInfo=TypeInfo(UInt32)
          then value := aValue.bb_read_uint32(aCursor)
          else if value.TypeInfo=TypeInfo(Byte)
          then value := aValue.bb_read_uint32(aCursor)
          else if value.TypeInfo=TypeInfo(Word)
          then value := aValue.bb_read_uint32(aCursor)
          else raise Exception.Create('Unsupported int type '+string(value.TypeInfo.Name)+' in TWDClassCacheEntry.setAttribute');
          field.RttiField.SetValue(aObject, value);
        end;
      tkEnumeration:
        begin
          CheckKey(wtVarint);
          //aEncodedAttributes.bb_read_uint32(cursor);
          value := TValue.FromOrdinal(value.TypeInfo, aValue.bb_read_uint32(aCursor));
          field.RttiField.SetValue(aObject, value);
        end;
      tkFloat:
        begin
          if value.TypeInfo=TypeInfo(Double)
          then value := aValue.bb_read_double(aCursor)
          else if value.TypeInfo=TypeInfo(Single)
          then value := aValue.bb_read_single(aCursor)
          else raise Exception.Create('Unsupported float type ('+string(value.TypeInfo.Name)+') in TWDClassCacheEntry.setAttribute');
          field.RttiField.SetValue(aObject, value);
        end;
//        tkChar, // AnsiChar, 1
//        tkWChar:
//          Result := Result+BBtag(kf.Key, value.GetReferenceToRawData^, value.DataSize);
      tkString:
        begin
          CheckKey(wtLengthDelimited);
          value := {PShortString(}aValue.bb_read_string(aCursor); // todo:
          field.RttiField.SetValue(aObject, value);
        end;
  //      tkSet: ;
      tkClass:
        begin
          CheckKey(wtLengthDelimited);
          len := aValue.bb_read_uint64(aCursor);
          // todo: more classes to decode
          {value := }
          (value.AsObject as TWDClass).Decode(aValue, aCursor, aCursor+Integer(len));
          //value := TWDGeometry.CreateFromBuffer(aValue, aCursor, aCursor+len);
          field.RttiField.SetValue(aObject, value);
        end;
  //      tkMethod: ;
      tkLString:
        begin
          CheckKey(wtLengthDelimited);
          value := aValue.bb_read_string(aCursor); // todo:
          field.RttiField.SetValue(aObject, value);
        end;
      tkWString:
        begin
          CheckKey(wtLengthDelimited);
          value := WideString(aValue.bb_read_string(aCursor));
          field.RttiField.SetValue(aObject, value);
        end;
  //      tkVariant: ;
//      tkArray:
//        Result := PrepareArrayElements(value, aTypeInfo);
      tkRecord:
        if value.TypeInfo=TypeInfo(TGUID) then
        begin
          CheckKey(wtLengthDelimited);
          value := TValue.From<TGUID>(aValue.bb_read_guid(aCursor));
          field.RttiField.SetValue(aObject, value);
        end
        else raise Exception.Create('Unsupported record type '+string(value.TypeInfo.Name)+' in TWDClassCacheEntry.setAttribute');
//        else Result := PrepareFields(value.GetReferenceToRawData, aTypeInfo);
  //      tkInterface: ;
      tkInt64:
        begin
          CheckKey(wtVarint);
          if value.TypeInfo=TypeInfo(Int64)
          then value := aValue.bb_read_int64(aCursor)
          else if value.TypeInfo=TypeInfo(UInt64)
          then value := aValue.bb_read_uint64(aCursor)
          else raise Exception.Create('Unsupported int64 type '+string(value.TypeInfo.Name)+' in TWDClassCacheEntry.setAttribute');
          field.RttiField.SetValue(aObject, value);
        end;
//      tkDynArray:
//        Result := PrepareDynamicArrayElements(value, aTypeInfo);
      tkUString:
        begin
          CheckKey(wtLengthDelimited);
          value := aValue.bb_read_string(aCursor);
          field.RttiField.SetValue(aObject, value);
        end;
  //      tkClassRef: ;
  //      tkPointer: ;
  //      tkProcedure: ;
    else
      raise Exception.Create('Unsupported field type ('+IntToStr(Ord(value.Kind))+') in TWDClassCacheEntry.setAttribute');
      //Result := False;
    end;
  end
  else Result := False; // aValue.bb_read_skip(aCursor, aKey and 7);
end;

function TWDClassCache.EncodeAttributes(aObject: TObject): TWDValue;
var
  kf: TPair<TWDKey, TWDClassCacheField>;
begin
  Result := '';
  for kf in fAttributesOnKey
  do Result := Result+TByteBuffer.bb_uint32(kf.Key)+getAttribute(aObject, kf.Key, kf.Value.RttiField);
end;

{ encode/decode }

{
function EncodeAttributes(aObject: TObject): TWDValue;
var
  wc: TWDClassCacheEntry;
begin
  if not classCache.TryGetValue(aObject.ClassType, wc) then
  begin
    wc := TWDClassCacheEntry.Create(aObject);
    classCache.Add(aObject.ClassType, wc);
  end;
  Result := wc.EncodeAttributes(aObject);
end;

procedure EncodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes);
var
  wc: TWDClassCacheEntry;
begin
  if not classCache.TryGetValue(aObject.ClassType, wc) then
  begin
    wc := TWDClassCacheEntry.Create(aObject);
    classCache.Add(aObject.ClassType, wc);
  end;
  wc.EncodeAttributes(aObject, aEncodedAttributes);
end;

procedure DecodeAttributes(aObject: TObject; const aEncodedAttributes: TWDValue);
var
  wc: TWDClassCacheEntry;
begin
  if not classCache.TryGetValue(aObject.ClassType, wc) then
  begin
    wc := TWDClassCacheEntry.Create(aObject);
    classCache.Add(aObject.ClassType, wc);
  end;
  wc.DecodeAttributes(aObject, aEncodedAttributes);
end;

procedure DecodeAttributes(aObject: TObject; aEncodedAttributes: TWDEncodedAttributes);
var
  wc: TWDClassCacheEntry;
begin
  if not classCache.TryGetValue(aObject.ClassType, wc) then
  begin
    wc := TWDClassCacheEntry.Create(aObject);
    classCache.Add(aObject.ClassType, wc);
  end;
  wc.DecodeAttributes(aObject, aEncodedAttributes);
end;
}

{ TWDGeometryBase }

procedure TWDGeometryBase.translate(dx, dy: Double);
begin
  // nothing to translate
end;

{ TWDGeometryPoint }
{
class function TWDGeometryPoint.CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  Result := TWDGeometryPoint.Create;
  Result.Decode(aBuffer, aCursor, aLimit);
end;
}

function TWDGeometryPoint.Copy: TWDGeometryBase;
begin
  Result := TWDGeometryPoint.Create(x,y,z);
end;

constructor TWDGeometryPoint.Create(ax, ay, az: Double);
begin
  inherited Create;
  x := ax;
  y := ay;
  z := az;
end;

function ParseCoordinate(const aCoordinate: string): Double;
begin
  try
    Result := Double.Parse(aCoordinate.Substring(4, aCoordinate.Length-5), dotFormat);
  except
    Result := Double.NaN;
  end;
end;

constructor TWDGeometryPoint.CreateFromSVGPath(const aSVG: string);
var
  s: TArray<string>;
  x, y, z: Double;
begin
  s := aSVG.Split([' ']);
  if length(s)>0
  then x := ParseCoordinate(s[0])
  else x := Double.NaN;
  if length(s)>1
  then y := -ParseCoordinate(s[1]) // todo: orientation in y is oposite to geojson?
  else y := Double.NaN;
  if length(s)>2
  then z := ParseCoordinate(s[2])
  else z := Double.NaN;
  create(x, y, z);
end;

constructor TWDGeometryPoint.Create;
begin
  inherited Create;
  x := NaN;
  y := NaN;
  z := NaN;
end;

function TWDGeometryPoint.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  if aCursor<aLimit
  then x := aBuffer.bb_read_double(aCursor)
  else x := NaN;
  if aCursor<aLimit
  then y := aBuffer.bb_read_double(aCursor)
  else y := NaN;
  if aCursor<aLimit
  then z := aBuffer.bb_read_double(aCursor)
  else z := NaN;
  Result := Self;
end;

function TWDGeometryPoint.Encode: TWDValue;
begin
  if IsNaN(z)
  then Result :=
         TByteBuffer.bb_bytes(x, SizeOf(x))+
         TByteBuffer.bb_bytes(y, SizeOf(y))
  else Result :=
         TByteBuffer.bb_bytes(x, SizeOf(x))+
         TByteBuffer.bb_bytes(y, SizeOf(y))+
         TByteBuffer.bb_bytes(z, SizeOf(z));
end;

function TWDGeometryPoint.getJSON2D(const aType: string): string;
begin
  // todo: ignore aType, always Point, check?
  Result := '['+DoubleToJSON(x)+','+DoubleToJSON(y)+']';
end;

function TWDGeometryPoint.getJSON2DSide(const aSide: Integer; const aFactor: Double): string;
begin
  raise Exception.Create('Cannot getJSON2DSide on point geometry');
  result := '';
end;

function TWDGeometryPoint.getJSONLatLon(const aType: string): string;
begin
  Result := '['+DoubleToJSON(y)+','+DoubleToJSON(x)+']'; // lat, lon ie reverse
end;

procedure TWDGeometryPoint.translate(dx, dy: Double);
begin
  x := x+dx;
  y := y+dy;
end;
{****
class function TWDGeometryPoint.WorldType: TWDWorldType;
begin
  Result := wdkGeometryPoint;
end;
}
{ TWDPoint2D }



class function TWDPoint2D.Create(aX, aY: Double): TWDPoint2D;
begin
  Result.x := aX;
  Result.y := aY;
end;

class function TWDPoint2D.Create(aPoint: TWDGeometryPoint): TWDPoint2D;
begin
  Result.x := aPoint.x;
  Result.y := aPoint.y;
end;

// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
// http://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect

class function TWDPoint2D.ccw(const A, B, C: TWDPoint2D): Double; // ie area, positive = ccw
begin
  Result := (C.y-A.y) * (B.x-A.x) - (B.y-A.y) * (C.x-A.x);
end;

class function TWDPoint2D.intersect(const A, B, C, D: TWDPoint2D): Boolean;
var
  acd: Double;
  bcd: Double;
  abc: Double;
  abd: Double;
begin
  abc := ccw(A, B, C);
  abd := ccw(A, B, D);
  // Return true if line segments AB and CD intersect
  if (abc=0) and (abd=0)
  then Result := False // collinear
  else
  begin
    acd := ccw(A, C, D);
    bcd := ccw(B, C, D);
    Result := ((acd>0) <> (bcd>0)) and ((abc>0) <> (abd>0));
  end;
end;


{ TWDLine2D }

class function TWDLine2D.Create(aX1, aY1, aX2, aY2: Double): TWDLine2D;
begin
  Result.p1 := TWDPoint2D.Create(aX1, aY1);
  Result.p2 := TWDPoint2D.Create(aX2, aY2);
end;

class function TWDLine2D.Create(aP1, aP2: TWDGeometryPoint): TWDLine2D;
begin
  Result.p1 := TWDPoint2D.Create(aP1);
  Result.p2 := TWDPoint2D.Create(ap2);
end;

class function TWDLine2D.intersect(const L1, L2: TWDLine2D): Boolean;
begin
  // Return true if line segments L1 and L2 intersect
//  Result :=
//    (TWDPoint2D.ccw(L1.p1, L2.p1, L2.p2) <> TWDPoint2D.ccw(L1.p2, L2.p1, L2.p2)) and
//    (TWDPoint2D.ccw(L1.p1, L1.p2, L2.p1) <> TWDPoint2D.ccw(L1.p1, L1.p2, L2.p2));
  Result := TWDPoint2D.intersect(L1.p1, L1.p2, L2.p1, L2.p2);
end;


{ TGeoColors }

class function TGeoColors.Create(aFillColor, aOutlineColor, aFill2Color: TAlphaRGBPixel): TGeoColors;
begin
  Result.fillColor := aFillColor;
  Result.outlineColor := aOutlineColor;
  Result.fill2Color := aFill2Color;
end;

class function TGeoColors.Create(aJSON: TJSONObject; aDefault: TAlphaRGBPixel): TGeoColors;
var
  json: TJSONValue;
begin
  // aDefault is only used for the fill color, rest is 0 (transparent)
  if aJSON.TryGetValue<TJSONValue>('fill', json)
  then Result.fillColor := JSONToAlphaColor(json, aDefault)
  else Result.fillColor := aDefault;
  if aJSON.TryGetValue<TJSONValue>('outline', json)
  then Result.outlineColor := JSONToAlphaColor(json, 0)
  else Result.outlineColor := 0;
  if aJSON.TryGetValue<TJSONValue>('fill2', json)
  then Result.fill2Color := JSONToAlphaColor(json, 0)
  else Result.fill2Color := 0;
end;

class function TGeoColors.Create(aJSON: TJSONValue; const aPath: string; aDefault: TAlphaRGBPixel): TGeoColors;
var
  jsonValue: TJSONValue;
begin
  if aJSON is TJSONString
  then Result := Create(StringToAlphaColor((aJSON as TJSONString).Value))
  else if aJSON is TJSONObject then
  begin
    if aPath<>'' then
    begin
      if (aJSON as TJSONObject).TryGetValue<TJSONValue>(aPath, jsonValue)
      then Result := Create(jsonValue, '', aDefault)
      else Result := Create(aDefault);
    end
    else Result := Create(aJSON as TJSONObject, aDefault)
  end
  else Result := Create(aDefault);
end;

function TGeoColors.mainColor(aDefaultColor: TAlphaRGBPixel): TAlphaRGBPixel;
begin
  if fillColor<>0
  then Result := fillColor
  else if outlineColor<>0
  then Result := outlineColor
  else if fill2Color<>0
  then Result := fill2Color
  else Result := aDefaultColor;
end;

function TGeoColors.toJSON: string;

  procedure AddColor(const aFieldPrefix: string; aColor: TAlphaRGBPixel);
  begin
    if aColor<>0 then
    begin
      if Result<>''
      then Result := Result+',';
      if ((aColor AND $FF000000) <> $FF000000) and ((aColor AND $FF000000) <> $00000000)
      then Result := Result+'"'+aFieldPrefix+'Opacity":'+ OpacityToJSON(aColor)+'",';
      Result := Result+'"'+aFieldPrefix+'Color":"'+ColorToJSON(aColor)+'"';
    end;
  end;

begin
  Result := '';
  AddColor('fill', fillColor);
  AddColor('outline', outlineColor);
  AddColor('fill2', fill2Color);
end;

{ TWDGeometryPart }

procedure TWDGeometryPart.AddPoint(x, y, z: Double);
var
  point: TWDGeometryPoint;
begin
  point := TWDGeometryPoint.Create;
  point.x := x;
  point.y := y;
  point.z := z;
  fPoints.Add(point);
end;

function TWDGeometryPart.cn_PnPoly(x, y: Double): Boolean;
var
  cn: Integer;
  i: Integer;
  vt: Double;
begin
  cn := 0; // the  crossing number counter
  // loop through all edges of the polygon
  for i := 0 to points.Count-2 do
  begin    // edge from V[i]  to V[i+1]
    if (((points[i].y <= y) and (points[i+1].y >  y)) or     // an upward crossing
        ((points[i].y >  y) and (points[i+1].y <= y))) then  // a downward crossing
    begin
      // compute  the actual edge-ray intersect x-coordinate
      vt := (y  - points[i].y) / (points[i+1].y - points[i].y);
      if (x < points[i].x + vt * (points[i+1].x - points[i].x)) // P.x < intersect
      then cn := cn+1; // a valid crossing of y=P.y right of P.x
    end;
  end;
  Result := (cn AND 1)=1; // 0 if even (out), and 1 if  odd (in)
end;

constructor TWDGeometryPart.Create;
begin
  inherited Create;
  fPoints := TObjectList<TWDGeometryPoint>.Create;
end;
{
class function TWDGeometryPart.CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  Result := TWDGeometryPart.Create;
  Result.Decode(aBuffer, aCursor, aLimit);
end;
}
function TWDGeometryPart.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
var
  len: UInt64;
  point: TWDGeometryPoint;
begin
  while aCursor<aLimit do
  begin
    len := aBuffer.bb_read_uint64(aCursor);
    if len>0 then
    begin
      point := TWDGeometryPoint.Create;
      try
        point.Decode(aBuffer, aCursor, aCursor+Integer(len));
      finally
        points.Add(point);
      end;
    end;
  end;
  Result := Self;
end;

destructor TWDGeometryPart.Destroy;
begin
  FreeAndNil(fPoints);
  inherited;
end;

function TWDGeometryPart.Encode: TWDValue;
var
  point: TWDGeometryPoint;
  pointBuffer: TWDValue;
begin
  Result := '';
  for point in points do
  begin
    pointBuffer := point.Encode;
    Result := Result+TByteBuffer.bb_uint64(Length(pointBuffer))+pointBuffer;
  end;
end;

function TWDGeometryPart.getJSON2D: string;
var
  i: Integer;
begin
  Result := '[';
  for i := 0 to fPoints.Count-1 do
  begin
    if i>0
    then Result := Result+',';
    Result := Result+fPoints[i].JSON2D[gtPoint];
  end;
  Result := Result+']';
end;

function TWDGeometryPart.getJSONLatLon: string;
var
  i: Integer;
begin
  Result := '[';
  for i := 0 to fPoints.Count-1 do
  begin
    if i>0
    then Result := Result+',';
    Result := Result+fPoints[i].JSONLatLon[gtPoint];
  end;
  Result := Result+']';
end;

function TWDGeometryPart.getJSON2DSide(const aSide: Integer; const aFactor: Double): string;
var
  p1, p2, v1, n1, p3, p4: TWDGeometryPoint;
  length: Double;
begin
  Result := '';
  if (fPoints.Count > 1) and (aSide <> 0) then
  begin
    if aSide > 0 then
    begin
      p1 := fPoints[fPoints.Count - 2];
      p2 := fPoints[fPoints.Count - 1];
    end
    else
    begin
      p1 := fPoints[1];
      p2 := fPoints[0];
    end;
    v1 := TWDGeometryPoint.Create(  //create 45 deg vector
      (p1.x - p2.x) + (p1.y - p2.y),
      (p2.x - p1.x) - (p2.y - p1.y),
      0);
    try
      length := Sqrt((v1.x * v1.x) + (v1.y * v1.y));
      n1 := TWDGeometryPoint.Create(v1.x / length, v1.y / length, 0); //create normal vector
      try
        p3 := TWDGeometryPoint.Create(p2.x + (n1.x * aFactor), p2.y + (n1.y * aFactor), 0);
        try
          p4 := TWDGeometryPoint.Create(p2.x - (n1.y * aFactor), p2.y + (n1.x * aFactor), 0);
          try
            Result := Result + p3.JSON2D[gtPoint] + ',' + p2.JSON2D[gtPoint] + ',' + p4.JSON2D[gtPoint];
          finally
            p4.Free;
          end;
        finally
          p3.Free;
        end;
      finally
        n1.Free;
      end;
    finally
      //TODO: can we do memory management like this?
      v1.Free;
    end;
  end;
end;

procedure TWDGeometryPart.translate(dx, dy: Double);
var
  p: TWDGeometryPoint;
begin
  for p in fPoints
  do p.translate(dx,dy);
end;

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//    Input:  line segment from L0 to L1, point x,y
//    Return: >0 for x,y left of the line through L0 and L1
//            =0 for x,y on the line
//            <0 for x,y  right of the line
//    See: Algorithm 1 "Area of Triangles and Polygons"
function isLeft(const L0, L1: TWDGeometryPoint; x, y: Double): Double; inline;
begin
  Result := (L1.x - L0.x) * (y - L0.y) - (x -  L0.x) * (L1.y - L0.y);
end;

(*
// cn_PnPoly(): crossing number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  0 = outside, 1 = inside
// This code is patterned after [Franklin, 2000]
function cn_PnPoly(const P: TWDGeometryPoint; const V: TArray<TWDGeometryPoint>): Integer;
var
  cn: Integer;
  i: Integer;
  vt: Double;
begin
  cn := 0;    // the  crossing number counter
  // loop through all edges of the polygon
  for i := 0 to length(V)-1 do
  begin    // edge from V[i]  to V[i+1]
    if (((V[i].y <= P.y) and (V[i+1].y >  P.y)) or     // an upward crossing
        ((V[i].y >  P.y) and (V[i+1].y <= P.y))) then  // a downward crossing
    begin
      // compute  the actual edge-ray intersect x-coordinate
      vt := (P.y  - V[i].y) / (V[i+1].y - V[i].y);
      if (P.x < V[i].x + vt * (V[i+1].x - V[i].x)) // P.x < intersect
      then cn := cn+1; // a valid crossing of y=P.y right of P.x
    end;
  end;
  Result := cn AND 1;    // 0 if even (out), and 1 if  odd (in)
end;

// wn_PnPoly(): winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)
function wn_PnPoly(const p: TWDGeometryPoint; const V: TArray<TWDGeometryPoint>): Integer;
var
  wn: Integer;
  i: Integer;
begin
  wn := 0; // the  winding number counter
  // loop through all edges of the polygon
  for i := 0 to length(V)-1 do    // edge from V[i] to  V[i+1]
  begin
    if (V[i].y <= P.y) then           // start y <= P.y
    begin
      if (V[i+1].y  > P.y)       // an upward crossing
      then if (isLeft(V[i], V[i+1], P) > 0)  // P left of  edge
           then wn := wn+1;            // have  a valid up intersect
    end
    else                         // start y > P.y (no test needed)
    begin
      if (V[i+1].y  <= P.y)      // a downward crossing
      then if (isLeft(V[i], V[i+1], P) < 0)  // P right of  edge
           then wn := wn-1;            // have  a valid down intersect
    end;
  end;
  Result := wn;
end;
*)

function TWDGeometryPart.wn_PnPoly(x, y: Double): Integer;
var
  wn: Integer;
  i: Integer;
begin
  wn := 0; // the  winding number counter
  // loop through all edges of the polygon
  for i := 0 to points.count-2 do    // edge from points[i] to  points[i+1]
  begin
    if (points[i].y <= y) then       // start y <= P.y
    begin
      if (points[i+1].y  > y)        // an upward crossing
      then if (isLeft(points[i], points[i+1], x, y) > 0)  // x,y left of  edge
           then wn := wn+1;          // have  a valid up intersect
    end
    else                             // start y > P.y (no test needed)
    begin
      if (points[i+1].y  <= y)       // a downward crossing
      then if (isLeft(points[i], points[i+1], x, y) < 0)  // x,y right of  edge
           then wn := wn-1;          // have  a valid down intersect
    end;
  end;
  Result := wn; // wn = the winding number (=0 only when x,y is outside)
end;

{****
class function TWDGeometryPart.WorldType: TWDWorldType;
begin
  Result := wdkGeometryPart;
end;
}
{ TWDGeometry }

function TWDGeometry.AddPart: TWDGeometryPart;
begin
  Result := TWDGeometryPart.Create;
  parts.Add(Result);
end;

procedure TWDGeometry.AddPoint(x, y, z: Double);
var
  part: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  if parts.Count=0
  then part := AddPart // new
  else part := parts.Last;
  point := TWDGeometryPoint.Create;
  point.x := x;
  point.y := y;
  point.z := z;
  part.points.Add(point);
end;

procedure TWDGeometry.AddPoint(aPoint: TWDGeometryPoint);
begin
  AddPoint(aPoint.x, aPoint.y, aPoint.z);
end;

function TWDGeometry.Copy: TWDGeometryBase;
var
  part, part2: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  Result := TWDGeometry.Create;
  for part in fParts do
  begin
    part2 := TWDGeometryPart.Create;

    for point in part.fPoints do
      part2.AddPoint(point.x, point.y, point.z);

    (Result as TWDGeometry).fParts.Add(part2);
  end;
end;

constructor TWDGeometry.Create;
begin
  inherited Create;
  fParts := TObjectList<TWDGeometryPart>.Create;
end;

constructor TWDGeometry.CreateFromSVGPath(const aSVGPath: string);
var
  elements: TArray<string>;
  i: Integer;
  e: string;
  x, y: Double;
  command: Integer;
begin
  Create;
  // "M -0.395195537337987 -39.477414746610833 L -0.395540696741319 -39.477884184697793 -0.395547058653566 -39.477891264070308 -0.395556723160279 -39.477897066138951 -0.395565187274533 -39.477900769276062 -0.395576390026876 -39.477902732028269 -0.39559012262954  (...)"
  elements := aSVGPath.Split([' ']);
  x := Double.NaN;
  for i := 0 to Length(elements)-1 do
  begin
    e := elements[i];
    command := Pos(System.copy(e,1,1), 'MmLlZz');
    if command<=0 then
    begin
      // must be coordinates
      if IsNaN(x)
      then x := Double.Parse(e, dotFormat)
      else
      begin
        y := -Double.Parse(e, dotFormat);  // todo: orientation in y is oposite to geojson?
        // handle complete point
        AddPoint(x, y, Double.NaN);
        x := Double.NaN;
      end;
    end
    else
    begin
      case command of
        1,2:
          begin
            AddPart;
            x := Double.NaN; // signal first coordinate
          end;
        3,4:
          begin
            x := Double.NaN; // signal first coordinate
          end;
        5,6:
          begin
            if parts.Count>0
            then AddPoint(parts.Last.points.first);
          end;
      end;
    end;
  end;
end;

{
class function TWDGeometry.CreateFromBuffer(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  Result := TWDGeometry.Create;
  Result.Decode(aBuffer, aCursor, aLimit);
end;
}
function TWDGeometry.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
var
  len: UInt64;
  part: TWDGeometryPart;
begin
  while aCursor<aLimit do
  begin
    len := aBuffer.bb_read_uint64(aCursor);
    if len>0 then
    begin
      part := TWDGeometryPart.Create;
      try
        part.Decode(aBuffer, aCursor, aCursor+Integer(len));
      finally
        parts.Add(part);
      end;
    end;
  end;
  Result := Self;
end;

destructor TWDGeometry.Destroy;
begin
  FreeAndNil(fParts);
  inherited;
end;

function TWDGeometry.Encode: TWDValue;
var
  part: TWDGeometryPart;
  partBuffer: TWDValue;
begin
  Result := '';
  for part in parts do
  begin
    partBuffer := part.Encode;
    Result := Result+TByteBuffer.bb_uint64(Length(partBuffer))+partBuffer;
  end;
end;

function TWDGeometry.getJSON2D(const aType: string): string;
var
  i: Integer;
begin
  if fParts.Count=0
  then Result := '"[]'
  else
  begin
    if aType=gtPoint then
    begin
      if fParts[0].fPoints.Count>0
      then Result := fParts[0].fPoints[0].JSON2D[aType]
      else Result := '[]'
    end
    else
    begin
      if (aType=gtLineString) or (aType=gtMultiPoint)
      then Result := ''+fParts[0].JSON2D // must be single part
      else
      begin
        Result := '';
        for i := 0 to fParts.Count-1 do
        begin
          if i>0
          then Result := Result+',';
          Result := Result+fParts[i].JSON2D;
        end;
        if (aType=gtPolygon) or (aType=gtMultiLineString)
        then Result := '['+Result+']' // can be multiple part
        else Result := '[['+Result+']]'; // todo: asume MultiPolygon; how to handle extra layer?
      end;
    end;
  end;
end;

function TWDGeometry.getJSON2DSide(const aSide: Integer; const aFactor: Double): string;
begin
  Result := '';
  if fParts.Count > 0 then //for now we assume gtLineString or gtMultiPoint for sides geometry so only 1 part!
   Result := Result + fParts[0].JSON2DSide[aSide, aFactor];
end;

function TWDGeometry.getJSONLatLon(const aType: string): string;
var
  i: Integer;
begin
  if fParts.Count=0
  then Result := '"[]'
  else
  begin
    if aType=gtPoint then
    begin
      if fParts[0].fPoints.Count>0
      then Result := fParts[0].fPoints[0].JSONLatLon[aType]
      else Result := '[]'
    end
    else
    begin
      if (aType=gtLineString) or (aType=gtMultiPoint)
      then Result := ''+fParts[0].JSONLatLon // must be single part
      else
      begin
        Result := '';
        for i := 0 to fParts.Count-1 do
        begin
          if i>0
          then Result := Result+',';
          Result := Result+fParts[i].JSONLatLon;
        end;
        if (aType=gtPolygon) or (aType=gtMultiLineString)
        then Result := '['+Result+']' // can be multiple part
        else Result := '[['+Result+']]'; // todo: asume MultiPolygon; how to handle extra layer?
      end;
    end;
  end;
end;

function TWDGeometry.intersectsSortOf(const aLine: TWDLine2D): Boolean;
var
  part: TWDGeometryPart;
  curPoint, prevPoint: TWDGeometryPoint;
  p: Integer;
  line: TWDLine2D;
begin
  Result := False;
  for part in parts do
  begin
    if part.points.Count>0 then
    begin
      curPoint := part.points[0];
      for p := 1 to part.points.Count-1 do
      begin
        prevPoint := curPoint;
        curPoint := part.points[p];
        line := TWDLine2D.Create(prevPoint, curPoint);
        if TWDLine2D.intersect(line, aLine)
        then exit(True);
      end;
    end;
  end;
end;

function TWDGeometry.intersectsSortOf(aGeometry: TWDGeometry): Boolean;
// todo: does not seem to work correctly, to many hits
var
  part: TWDGeometryPart;
  curPoint, prevPoint: TWDGeometryPoint;
  p: Integer;
  line: TWDLine2D;
begin
  Result := False;
  for part in parts do
  begin
    if part.points.Count>0 then
    begin
      curPoint := part.points[0];
      for p := 1 to part.points.Count-1 do
      begin
        prevPoint := curPoint;
        curPoint := part.points[p];
        line := TWDLine2D.Create(prevPoint, curPoint);
        if aGeometry.intersectsSortOf(line)
        then exit(True);
      end;
    end;
  end;
end;

function TWDGeometry.PointInAnyPart(x, y: Double): Boolean;
var
  part: TWDGeometryPart;
begin
  Result := False;
  for part in parts do
  begin
    if part.cn_PnPoly(x,y) then
    begin
      Result := True;
      break;
    end;
  end;
end;

function TWDGeometry.PointInFirstPart(x, y: Double): Boolean;
begin
  if parts.Count>0
  then Result := parts.First.cn_PnPoly(x,y)
  else Result := False;
end;

function TWDGeometry.PointsInAnyPart(aGeometry: TWDGeometry): Boolean;
var
  part: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  // sentinel
  Result := False;
  for part in aGeometry.parts do
  begin
    for point in part.points do
    begin
      if PointInFirstPart(point.x, point.y)
      then exit(True);
    end;
  end;
end;

procedure TWDGeometry.translate(dx, dy: Double);
var
  part: TWDGeometryPart;
begin
  for part in fParts
  do part.translate(dx, dy);
end;
{****
class function TWDGeometry.WorldType: TWDWorldType;
begin
  Result := wdkGeometry;
end;
}
{ TExtent }

//function TExtent.AsStringInt: string;
//begin
//  Result := IntToStr(Round(xMin))+' x '+IntToStr(Round(yMin))+' - '+IntToStr(Round(xMax))+' x '+IntToStr(Round(yMax))
//end;

function TWDExtent.CenterX: Double;
begin
  Result := (xMin+xMax)/2;
end;

function TWDExtent.CenterY: Double;
begin
  Result := (yMin+yMax)/2;
end;

function TWDExtent.Contains(x, y: Double): Boolean;
begin
  if not IsEmpty
  then Result :=
        (x >= Self.xMin) and
        (x <= Self.xMax) and
        (y >= Self.yMin) and
        (y <= Self.xMax)
  else Result := False;
end;

class function TWDExtent.Create(aXMin, aYMin, aXMax, aYMax: Double): TWDExtent;
begin
  Result.Init;
  Result.xMin := aXMin;
  Result.yMin := aYMin;
  Result.xMax := aXMax;
  Result.yMax := aYMax;
end;

class function TWDExtent.Create: TWDExtent;
begin
  Result.Init;
end;

procedure TWDExtent.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer);
begin
  XMin := aBuffer.bb_read_double(aCursor);
  YMin := aBuffer.bb_read_double(aCursor);
  XMax := aBuffer.bb_read_double(aCursor);
  YMax := aBuffer.bb_read_double(aCursor);
  if aCursor>aLimit
  then Exception.Create('TWDExtent.Decode read over limit ('+aCursor.toString+', '+aLimit.toString);
  aCursor := aLimit; // todo: correct? in case we have not read enough or just more was send
end;

procedure TWDExtent.Init;
begin
  xMin := NaN;
  yMin := NaN;
  xMax := NaN;
  yMax := NaN;
end;

function TWDExtent.Encode: TWDValue;
begin
  Result :=
    TByteBuffer.bb_bytes(xMin, sizeOf(xMin))+
    TByteBuffer.bb_bytes(yMin, sizeOf(yMin))+
    TByteBuffer.bb_bytes(xMax, sizeOf(xMax))+
    TByteBuffer.bb_bytes(yMax, sizeOf(yMax));
end;

function TWDExtent.Expand(aGeometry: TWDGeometry): Boolean;
var
  part: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  Result := False;
  for part in aGeometry.parts do
  begin
    for point in part.points do
    begin
      if Expand(point.x, point.y)
      then Result := True;
    end;
  end;
end;

function TWDExtent.Expand(aExtent: TWDExtent): Boolean;
begin
  if not aExtent.IsEmpty then
  begin
    Result := Expand(aExtent.XMin, aExtent.YMin);
    if Expand(aExtent.XMax, aExtent.YMax)
    then Result := True;
  end
  else Result := False;
end;

function TWDExtent.Expand(x, y: Double): Boolean;
begin
  Result := False;
  if isEmpty then
  begin
    if not (IsNaN(x) and IsNan(y)) then
    begin
      Init(x, y);
      Result := True;
    end;
  end
  else
  begin
    if xMin>x then
    begin
      xMin := x;
      Result := True;
    end;
    if xMax<x then
    begin
      xMax := x;
      Result := True;
    end;
    if yMin>y then
    begin
      yMin := y;
      Result := True;
    end;
    if yMax<y then
    begin
      yMax := y;
      Result := True;
    end;
  end;
end;

class function TWDExtent.FromGeometry(aGeometry: TWDGeometry): TWDExtent;
var
  part: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  Result.Init;
  for part in aGeometry.parts do
  begin
    for point in part.points do
    begin
      if Result.IsEmpty
      then Result.Init(point.X, point.Y)
      else Result.Expand(point.X, point.Y);
    end;
  end;
end;

procedure TWDExtent.Init(x, y, aWidth, aHeight: Double);
begin
  xMin := x;
  xMax := xMin+aWidth;
  yMin := y;
  yMax := y+aHeight;
end;

procedure TWDExtent.Init(x, y: Double);
begin
  xMin := x;
  xMax := x;
  yMin := y;
  yMax := y;
end;

function TWDExtent.Intersects(const aExtent: TWDExtent): Boolean;
begin
  if not (Self.IsEmpty or aExtent.IsEmpty)
  then Result :=
         (Self.XMin <= aExtent.XMax) and
         (Self.XMax >= aExtent.XMin) and
         (Self.YMin <= aExtent.YMax) and
         (Self.YMax >= aExtent.YMin)
  else Result := False;
end;

function TWDExtent.IsEmpty: Boolean;
begin
  Result := IsNaN(xMin);
end;

procedure TWDExtent.translate(dx, dy: Double);
begin
  xMin := xMin+dx;
  yMin := yMin+dy;
  xMax := xMax+dx;
  yMax := yMax+dy;
end;

{
function TWDExtent.Square: TWDExtent;
var
  d: TDistanceLatLon;
  w, h: Double;
begin
  // make square in meters (relative from center of extent) containing the original extent
  d := TDistanceLatLon.Create(YMin, YMax);
  w := d.m_per_deg_lon*(XMax-XMin);
  h := d.m_per_deg_lat*(YMax-YMin);
  if Abs(w)>=Abs(h) then
  begin
    Result.XMin := XMin;
    Result.XMax := XMax;
    h := w/d.m_per_deg_lat;
    Result.YMin := ((YMin+YMax)/2)-(h/2);
    Result.YMax := ((YMin+YMax)/2)+(h/2);
  end
  else
  begin
    Result.YMin := YMin;
    Result.YMax := YMax;
    w := h/d.m_per_deg_lat;
    Result.XMin := ((XMin+XMax)/2)-(w/2);
    Result.XMax := ((XMin+XMax)/2)+(w/2);
  end;
end;
}

{ TWDPalette }

function TWDPalette.Clone: TWDPalette;
begin
  if Assigned(Self)
  then Result := Self._clone
  else Result := nil;
end;

constructor TWDPalette.Create(const aDescription: string);
begin
  inherited Create;
  fDescription := aDescription;
end;

function TWDPalette.Decode(const aBuffer: TWDValue; var aCursor: Integer; aLimit: Integer): TWDClass;
begin
  fDescription := aBuffer.bb_read_string(aCursor);
  Result := Self;
end;

function TWDPalette.Encode: TWDValue;
begin
  Result := TByteBuffer.bb_string(fDescription);
end;

//function TWDPalette.ValueToColorJSON(const aValue: Double): string;
//begin
//  Result := ColorToJSON(ValueToColor(aValue));
//end;


end.

