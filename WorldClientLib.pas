unit WorldClientLib;

interface

uses
  Logger,
  imb4,
  WorldDataCode,
  WinApi.Windows, System.Math, System.SysUtils, System.Generics.Collections;

const
  RemoteHostSwitch = 'RemoteHost';
  RemotePortSwitch = 'RemotePort';
  PrefixSwitch = 'Prefix';
    DefaultPrefix = 'World';

  DefaultMaxPacketSize = 4*1024;

  DefaultBinLoadTimeOut = 5*60;

type
  TWDDataSources = class; // forward

  TWDBin = class; // forward

  TWDCollection<T> = class; // forward

  TWDSchemaAttribute = class
  public
    key: TWDKey;
    name: string;
    worldFunctions: TWDFunctions;
    source: string;
  end;

  TWDSchemaCollection = class
  constructor Create;
  destructor Destroy; override;
  public
    function NextFreeKey(aKey: TWDKey): TWDKey;
  public
    attributesOnName: TObjectDictionary<string, TWDSchemaAttribute>; // owns
    attributesOnKey: TObjectDictionary<TWDKey, TWDSchemaAttribute>; // refs
  end;

  TWDSchema = TObjectDictionary<string, TWDSchemaCollection>;

  TWDCObjectUpdate = class
  constructor Create(aEventEntry: TEventEntry; aMaxPacketSize: Integer=DefaultMaxPacketSize);
  destructor Destroy; override;
  private
    fEventEntry: TEventEntry;
    fMaxPacketSize: Integer;
    fBuffer: TByteBuffer;
    fLastID: TWDID;
    fLastTimeStamp: Double;
  protected
    procedure setValueRaw(const aValue: RawByteString; aTimeStamp: Double=NaN);
    procedure CommitL;
  public
    procedure setID(const aID: TWDID);

    procedure setValue(aTag: UInt32; aValue: Double; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: Single; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: Int64; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: Int32; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: UInt64; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: UInt32; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; const aValue: RawByteString; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; const aValue: string; aTimeStamp: Double=NaN); overload;
    procedure setValue(aTag: UInt32; aValue: TWDClass; aTimeStamp: Double=NaN); overload;

    procedure Commit;
  end;

  TWDCOnTruncate<T> = reference to procedure(aCollection: TWDCollection<T>);
  TWDCOnObject<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; out aObject: T);
  TWDCOnNoObject<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID);
  TWDCOnAttribute<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; aKey: TWDKey; const aValue: TWDValue);

  TWDAttrProcBase<T> = class
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); virtual; abstract;
  end;

  TWDAttrProcDoubleProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: Double);

  TWDAttrProcDouble<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcDoubleProcessor<T>);
  private
    fProcessor: TWDAttrProcDoubleProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcSingleProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: Single);

  TWDAttrProcSingle<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcSingleProcessor<T>);
  private
    fProcessor: TWDAttrProcSingleProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcStringProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aValue: String);

  TWDAttrProcString<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcStringProcessor<T>);
  private
    fProcessor: TWDAttrProcStringProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcTByteBufferProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: TByteBuffer);

  TWDAttrProcTByteBuffer<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcTByteBufferProcessor<T>);
  private
    fProcessor: TWDAttrProcTByteBufferProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcBooleanProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: Boolean);

  TWDAttrProcBoolean<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcBooleanProcessor<T>);
  private
    fProcessor: TWDAttrProcBooleanProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcUInt32Processor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: UInt32);

  TWDAttrProcUInt32<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcUInt32Processor<T>);
  private
    fProcessor: TWDAttrProcUInt32Processor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcInt32Processor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: Int32);

  TWDAttrProcInt32<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcInt32Processor<T>);
  private
    fProcessor: TWDAttrProcInt32Processor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcUInt64Processor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: UInt64);

  TWDAttrProcUInt64<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcUInt64Processor<T>);
  private
    fProcessor: TWDAttrProcUInt64Processor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcInt64Processor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; aValue: Int64);

  TWDAttrProcInt64<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcInt64Processor<T>);
  private
    fProcessor: TWDAttrProcInt64Processor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDAttrProcClassProcessor<T> = reference to procedure(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);

  TWDAttrProcClass<T> = class(TWDAttrProcBase<T>)
  constructor Create(const aProcessor: TWDAttrProcClassProcessor<T>);
  private
    fProcessor: TWDAttrProcClassProcessor<T>;
  public
    procedure ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer); override;
  end;

  TWDCollection<T> = class
  constructor Create(aBin: TWDBin; const aName: string; aSchemaCollection: TWDSchemaCollection);
  destructor Destroy; override;
  private
    fPrivateEvent: TEventEntry;
  protected
    fBin: TWDBin; // ref
    fName: string;
    fEvent: TEventEntry; // ref
    fSchemaCollection: TWDSchemaCollection; // ref
    fOnObject: TWDCOnObject<T>;
    fOnNoObject: TWDCOnNoObject<T>;
    fOnTruncate: TWDCOnTruncate<T>;
    fTagProcessors: TObjectDictionary<UInt32, TWDAttrProcBase<T>>;
    fObjects: TObjectDictionary<TWDID, T>;
    procedure HandleEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer); //virtual;
  public
    property name: string read fName;
    property event: TEventEntry read fEvent;
    property objects: TObjectDictionary<TWDID, T> read fObjects;
    property schemaCollection: TWDSchemaCollection read fSchemaCollection;

    property onObject: TWDCOnObject<T> read fOnObject write fOnObject;
    property onTruncate: TWDCOnTruncate<T> read fOnTruncate write fOnTruncate; // optional
    property onNoObject: TWDCOnNoObject<T> read fOnNoObject write fOnNoObject; // optional

    procedure RegisterAttributeClear();

    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcDoubleProcessor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcSingleProcessor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcStringProcessor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcTByteBufferProcessor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcBooleanProcessor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcUInt32Processor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcInt32Processor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcUInt64Processor<T>); overload;
    procedure RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcInt64Processor<T>); overload;

    procedure Inquire(const aFilter: string);
    procedure Truncate();
  end;

  TWDCollections = TObjectDictionary<string, TObject>;

  TWDBin = class
  constructor Create(aDataSources: TWDDataSources; const aBinID: TWDID; aLoaded: Boolean=False);
  destructor Destroy; override;
  private
    fDataSources: TWDDataSources;
    fEvent: TEventEntry;
    fBinID: TWDID;
    fLoaded: Boolean;
    fCollections: TWDCollections;
    fHandleBinEventRef: TOnEvent;
  private
    procedure handleBinEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    procedure handleResponse(const aResponse: TByteBuffer);
  protected
    procedure signalBinLoad(aEventEntry: TEventEntry; const aBinID: TWDID);
    procedure signalBinUnLoad(aEventEntry: TEventEntry; const aBinID: TWDID);
    procedure signalCollectionAdd(aEventEntry: TEventEntry; const aCollectionName: string);
  public
    property DataSources: TWDDataSources read fDataSources;
    property BinID: TWDID read fBinID;
    property Loaded: Boolean read fLoaded;
    property Collections: TWDCollections read fCollections;

    function Collection<T>(const aName: string): TWDCollection<T>;

    procedure Subscribe;
    procedure UnSubscribe;
    // load/unload does implicit subscribe/unsubscribe
    function Load(aTimeOut: Int64=DefaultBinLoadTimeOut): Boolean;
    procedure UnLoad;
  end;

  TWDBins = TObjectDictionary<TWDID, TWDBin>;

  TWDDataSources = class
  constructor Create(aConnection: TConnection);
  destructor Destroy; override;
  private
    fConnection: TConnection;
    fEvent: TEventEntry;
    fPrivateEvent: TEventEntry;
    fBins: TWDBins;
    fSources: TList<string>;
    fSchema: TWDSchema;

    fHandleDataSourcesEventRef: TOnEvent;
  private
    procedure handleDataSourcesEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    procedure handleResponse(const aResponse: TByteBuffer);
  protected
    // data source
    procedure signalInquireDataSources(aEventEntry: TEventEntry; const aReturnEventName: string='');
    procedure signalInquireBins(aEventEntry: TEventEntry; const aReturnEventName: string='');
    procedure signalInquireSchema(aEventEntry: TEventEntry; const aReturnEventName: string='');

    procedure signalSchemaAttribute(aEventEntry: TEventEntry; const aSchemaSource, aSchemaCollectionName: string; const aSchemaAttribute: TWDSchemaAttribute); overload;
    procedure signalSchemaAttribute(aEventEntry: TEventEntry; const aSchemaSource, aSchemaCollectionName: string; const aSchemaAttribute: TWDClassCacheField); overload;

    procedure signalBinCreate(aEventEntry: TEventEntry; const aBinID: TWDID; const aDescription: string; aParentID, aRefID: TWDID);
    procedure signalBinCopy(aEventEntry: TEventEntry; const aSrcID, aDstID: TWDID); overload;
    procedure signalBinDelete(aEventEntry: TEventEntry; const aBinID: TWDID);
  public
    property Connection: TConnection read fConnection;
    property Bins: TWDBins read fBins;
    property Sources: TList<string> read fSources;
    property Schema: TWDSchema read fSchema;

    function Bin(const aBinID: TWDID; aOnlyExisting: Boolean=True): TWDBin;
    function SchemaCollection(const aName: string; aOnlyExisting: Boolean=True): TWDSchemaCollection;
  end;

implementation

function ReadValue(const aPayload: TByteBuffer; var aCursor: Integer; aWireType: Integer): TWDValue;
var
  sv: Integer;
  len: UInt64;
begin
  case aWireType of
    wtVarInt:
      begin
        sv := aCursor;
        aPayload.bb_read_uint64(aCursor); // we only want the new cursor to store the buffer occupied by the varint
        result := copy(aPayload, sv+1, aCursor-sv);
      end;
    wt64Bit:
      begin
        result := copy(aPayload, aCursor+1, 8);
        Inc(aCursor, 8);
      end;
    wtLengthDelimited:
      begin
        sv := aCursor;
        len := aPayload.bb_read_uint64(aCursor);
        result := copy(aPayload, sv+1, Integer(len)+aCursor-sv);
        Inc(aCursor, len);
      end;
    wt32Bit:
      begin
        result := copy(aPayload, aCursor+1, 4);
        Inc(aCursor, 4);
      end
  else
    raise Exception.Create('Unsupported wire type ('+aWireType.ToString+') in ReadValue');
  end;
end;

{ TWDSchemaCollection }

constructor TWDSchemaCollection.Create;
begin
  inherited Create;
  attributesOnName := TObjectDictionary<string, TWDSchemaAttribute>.Create([doOwnsValues]);
  attributesOnKey := TObjectDictionary<TWDKey, TWDSchemaAttribute>.Create([]);
end;

destructor TWDSchemaCollection.Destroy;
begin
  FreeAndNil(attributesOnName);
  FreeAndNil(attributesOnKey);
  inherited;
end;

function TWDSchemaCollection.NextFreeKey(aKey: TWDKey): TWDKey;
var
  keywoWT: TWDKey;
  akp: TPair<string, TWDSchemaAttribute>;
  akpwoWT: TWDKey;
begin
  // calc given key without wire type
  keywoWT := aKey shr 3;
  // check to min value of key, (-1 because we are going to get the NEXT key)
  if keywoWT<icehAttributeBase-1
  then keywoWT := icehAttributeBase-1;
  // check for existing higher keys
  for akp in attributesOnName do
  begin
    akpwoWT := akp.Value.Key shr 3;
    if keywoWT<akpwoWT
    then keywoWT := akpwoWT;
  end;
  // rebuild key with wire type as the NEXT free key
  Result := ((keywoWT+1) shl 3) or (aKey and 7);
end;

{ TWDCollection }

constructor TWDCollection<T>.Create(aBin: TWDBin; const aName: string; aSchemaCollection: TWDSchemaCollection);
begin
  inherited Create;
  fObjects := TObjectDictionary<TWDID, T>.Create([doOwnsValues]);
  fTagProcessors := TObjectDictionary<UInt32, TWDAttrProcBase<T>>.Create([doOwnsValues]);
  fBin := aBin;
  fName := aName;
  fSchemaCollection := aSchemaCollection;
  fEvent := fBin.fEvent.connection.eventEntry(fBin.fEvent.eventName+'.'+fName, False).subscribe;
  fEvent.OnEvent.Add(HandleEvent);
  fPrivateEvent := fBin.DataSources.Connection.eventEntry(fBin.DataSources.Connection.privateEventName+'.'+fName, False).subscribe;
  fPrivateEvent.OnEvent.Add(HandleEvent);
end;

destructor TWDCollection<T>.Destroy;
begin
  if Assigned(fPrivateEvent) then
  begin
    fPrivateEvent.unSubscribe;
    fPrivateEvent := nil;
  end;
  if Assigned(fEvent) then
  begin
    fEvent.unSubscribe;
    fEvent := nil;
  end;
  fSchemaCollection := nil;
  fBin := nil;
  FreeAndNil(fTagProcessors);
  FreeAndNil(fObjects);
  inherited;
end;

procedure TWDCollection<T>.HandleEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  fieldInfo: UInt32;
  processor: TWDAttrProcBase<T>;
  id: TWDID;
  collectionName: string;
  obj: T;
begin
  // todo:
  //obj := nil;
  while aCursor<aLimit do
  begin
    fieldInfo := aBuffer.bb_read_UInt32(aCursor);
    case fieldInfo of
      wdatObjectID:
        begin
          id := aBuffer.bb_read_rawbytestring(aCursor);
          if Assigned(fOnObject) then
          begin
            TMonitor.Enter(Self);
            try
              if not fObjects.TryGetValue(id, obj) then
              begin
                fOnObject(Self, id, obj);
                fObjects.Add(id, obj);
              end;
            finally
              TMonitor.Exit(Self);
            end;
          end;
        end;
      wdatNoObjectID:
        begin
          id := aBuffer.bb_read_rawbytestring(aCursor);
          TMonitor.Enter(Self);
          try
            fOnNoObject(Self, id);
            fObjects.Remove(id);
          finally
            TMonitor.Exit(Self);
          end;
        end;
      wdatCollectionTruncate:
        begin
          collectionName := aBuffer.bb_read_string(aCursor);
          if (collectionName='') or (SameText(collectionName, fName)) then
          begin
            TMonitor.Enter(Self);
            try
              fOnTruncate(Self);
              fObjects.Clear;
            finally
              TMonitor.Exit(Self);
            end;
          end;
        end;
    else
      if fTagProcessors.TryGetValue(fieldInfo, processor) then
      begin
        // todo: really needed? if using smart pointers then only object itself has to be locked..
        TMonitor.Enter(Self);
        try
          processor.ProcessAttribute(Self, id, obj, fieldInfo shr 3, aBuffer, aCursor, aLimit);
        finally
          TMonitor.Exit(Self);
        end;
      end
      else aBuffer.bb_read_skip(aCursor, fieldInfo and 7);
    end;
  end;
end;

procedure TWDCollection<T>.Inquire(const aFilter: string);
begin
  fEvent.signalEvent(
      TByteBuffer.bb_tag_string(wdatReturnEventName shr 3, fPrivateEvent.eventName)+
      TByteBuffer.bb_tag_string(wdatObjectsInquire shr 3, aFilter));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcTByteBufferProcessor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtLengthDelimited, TWDAttrProcTByteBuffer<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcBooleanProcessor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtVarInt, TWDAttrProcBoolean<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcStringProcessor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtLengthDelimited, TWDAttrProcString<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcDoubleProcessor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wt64Bit, TWDAttrProcDouble<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcSingleProcessor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wt32Bit, TWDAttrProcSingle<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcUInt64Processor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtVarInt, TWDAttrProcUInt64<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcInt64Processor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtVarInt, TWDAttrProcInt64<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcUInt32Processor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtVarInt, TWDAttrProcUInt32<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttribute(aTag: UInt32; const aProcessor: TWDAttrProcInt32Processor<T>);
begin
  fTagProcessors.AddOrSetValue((aTag shl 3) or wtVarInt, TWDAttrProcInt32<T>.Create(aProcessor));
end;

procedure TWDCollection<T>.RegisterAttributeClear;
begin
  fTagProcessors.Clear;
end;

procedure TWDCollection<T>.Truncate;
begin
  fEvent.signalEvent(TByteBuffer.bb_tag_string(wdatCollectionTruncate shr 3, fName));
end;

{ TWDBin }

function TWDBin.Collection<T>(const aName: string): TWDCollection<T>;
var
  res: TObject;
begin
  TMonitor.Enter(fCollections);
  try
    if not fCollections.TryGetValue(aName, res) then
    begin
      Result := TWDCollection<T>.Create(Self, aName, fDataSources.SchemaCollection(aName));
      fCollections.Add(aName, Result);
    end
    else Result := res as TWDCollection<T>;
  finally
    TMonitor.Exit(fCollections);
  end;
end;

constructor TWDBin.Create(aDataSources: TWDDataSources; const aBinID: TWDID; aLoaded: Boolean);
begin
  inherited Create;
  fDataSources := aDataSources;
  fBinID := aBinID;
  fLoaded := aLoaded;
  fEvent := nil;
  fHandleBinEventRef := handleBinEvent;
  fCollections := TWDCollections.Create([doOwnsValues]);
  fHandleBinEventRef := handleBinEvent;
end;

destructor TWDBin.Destroy;
begin
  UnSubscribe;
  FreeAndNil(fCollections);
  inherited;
end;

procedure TWDBin.handleBinEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  key: TWDKey;
  binID: TWDID;
begin
  while aCursor<aLimit do
  begin
    key := aBuffer.bb_read_uint32(aCursor);
    case key of
      wdatBinLoaded,
      wdatBinUnLoaded:
        begin
          binID := aBuffer.bb_read_rawbytestring(aCursor);
          if (binID='') or (binID=fBinID)
          then fLoaded := key=wdatBinLoaded;
        end;
    else
      aBuffer.bb_read_skip(aCursor, key and 7); // 7290=911=wdatDataSource
    end;
  end;
end;

procedure TWDBin.handleResponse(const aResponse: TByteBuffer);
begin
  handleBinEvent(fEvent, aResponse, 0, length(aResponse));
end;

function TWDBin.Load(aTimeOut: Int64): Boolean;
var
  requestError: Integer;
  response: TByteBuffer;
begin
  Subscribe;
  // signalBinLoad(fEvent, fBinID); -> implement load request synchronously
  response := fEvent.Request(TByteBuffer.bb_tag_rawbytestring(wdatBinLoad shr 3, fBinID), requestError, aTimeOut);
  try
    if requestError=reOK then
    begin
      handleResponse(response);
      Result := True;
    end
    else Result := False;
  finally
    setLength(response, 0);
  end;
end;

procedure TWDBin.signalBinLoad(aEventEntry: TEventEntry; const aBinID: TWDID);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_rawbytestring(wdatBinLoad shr 3, aBinID));
end;

procedure TWDBin.signalBinUnLoad(aEventEntry: TEventEntry; const aBinID: TWDID);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_rawbytestring(wdatBinUnLoad shr 3, aBinID));
end;

procedure TWDBin.signalCollectionAdd(aEventEntry: TEventEntry; const aCollectionName: string);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_string(wdatCollectionAdd shr 3, aCollectionName));
end;

procedure TWDBin.Subscribe;
begin
  fEvent := fDataSources.Connection.eventEntry('Bins.'+string(UTF8String(fBinID))).subscribe;
  if not fEvent.OnEvent.Contains(fHandleBinEventRef)
  then fEvent.OnEvent.Add(fHandleBinEventRef);
end;

procedure TWDBin.UnLoad;
begin
  signalBinUnLoad(fDataSources.fEvent, fBinID);
  UnSubscribe;
end;

procedure TWDBin.UnSubscribe;
begin
  if Assigned(fEvent) and fEvent.OnEvent.Contains(fHandleBinEventRef)
  then fEvent.OnEvent.Remove(fHandleBinEventRef);
  fEvent := nil;
end;

{ TWDDataSources }

function TWDDataSources.Bin(const aBinID: TWDID; aOnlyExisting: Boolean): TWDBin;
begin
  TMonitor.Enter(fBins);
  try
    if not fBins.TryGetValue(aBinID, Result) then
    begin
      if not aOnlyExisting then
      begin
        Result := TWDBin.Create(Self, aBinID);
        fBins.Add(aBinID, Result);
      end
      else Result := Nil;
    end;
    if Assigned(Result)
    then Result.Subscribe;
  finally
    TMonitor.Exit(fBins);
  end;
end;

constructor TWDDataSources.Create(aConnection: TConnection);
var
  requestError: Integer;
//  response: TByteBuffer;
begin
  inherited Create;
  fHandleDataSourcesEventRef := handleDataSourcesEvent;
  fConnection := aConnection;
  fBins := TWDBins.Create([doOwnsValues]);
  fSources := TList<string>.Create;
  fSchema := TWDSchema.Create([doOwnsValues]);
  fPrivateEvent := fConnection.eventEntry(fConnection.privateEventName, False).subscribe;
  fPrivateEvent.OnEvent.Add(fHandleDataSourcesEventRef);
  fEvent := fConnection.eventEntry('DataSources').subscribe;
  fEvent.OnEvent.Add(fHandleDataSourcesEventRef);

  //signalInquireDataSources(fEvent, fPrivateEvent.eventName);
  //signalInquireBins(fEvent, fPrivateEvent.eventName);
  //signalInquireSchema(fEvent, fPrivateEvent.eventName);

//  response := fEvent.Request(TByteBuffer.bb_tag_string(wdatDataSourcesInquire shr 3, ''), requestError);
//  handleResponse(response);
//  setLength(response, 0);
//  response := fEvent.Request(TByteBuffer.bb_tag_string(wdatBinsInquire shr 3, ''), requestError);
//  handleResponse(response);
//  setLength(response, 0);
//  response := fEvent.Request(TByteBuffer.bb_tag_string(wdatSchemaInquire shr 3, ''), requestError);
//  handleResponse(response);
//  setLength(response, 0);

  handleResponse(fEvent.Request(TByteBuffer.bb_tag_string(wdatDataSourcesInquire shr 3, ''), requestError));
  handleResponse(fEvent.Request(TByteBuffer.bb_tag_string(wdatBinsInquire shr 3, ''), requestError));
  handleResponse(fEvent.Request(TByteBuffer.bb_tag_string(wdatSchemaInquire shr 3, ''), requestError));

end;

destructor TWDDataSources.Destroy;
begin
  if Assigned(fPrivateEvent) and fPrivateEvent.OnEvent.Contains(fHandleDataSourcesEventRef)
  then fPrivateEvent.OnEvent.Remove(fHandleDataSourcesEventRef);
  if Assigned(fEvent) and fEvent.OnEvent.Contains(fHandleDataSourcesEventRef)
  then fEvent.OnEvent.Remove(fHandleDataSourcesEventRef);
  FreeAndNil(fBins);
  FreeAndNil(fSources);
  FreeAndNil(fSchema);
  inherited;
end;

// todo: memory leak?
procedure TWDDataSources.handleDataSourcesEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  key: TWDKey;
  dataSourceEventName: string;
  binID: TWDID;
  bin: TWDBin;
  sSource: string;
  sCollectionName: string;
  sCollection: TWDSchemaCollection;
  schemaAttributeKey: TWDKey;
  sAttributeOnName: TWDSchemaAttribute;
  sAttributeName: string;
  sAttributeWorldFunction: TWDFunction;
  loaded: Boolean;
  sAttributeOnKey: TWDSchemaAttribute;
  cmp: Integer;
  collection: TObject;
  //ls: TWDClassCacheField;
begin
  try
    sSource := '';
    sCollectionName := '';
    sCollection := nil;
    sAttributeOnName := nil;
    sAttributeName := '';
    sAttributeWorldFunction := 0;
    while aCursor<aLimit do
    begin
      key := aBuffer.bb_read_uint32(aCursor);
      case key of
        wdatDataSource:
          begin
            dataSourceEventName := aBuffer.bb_read_string(aCursor);
            if fSources.IndexOf(dataSourceEventName)<0
            then fSources.Add(dataSourceEventName);
          end;
        wdatBinLoaded,
        wdatBinUnLoaded:
          begin
            binID := aBuffer.bb_read_rawbytestring(aCursor);
            loaded := key=wdatBinLoaded;
            TMonitor.Enter(fBins);
            try
              if not fBins.TryGetValue(binID, bin) then
              begin
                bin := TWDBin.Create(Self, binID, loaded);
                fBins.Add(binID, bin);
              end
              else bin.fLoaded := loaded;
            finally
              TMonitor.Exit(fBins);
            end;
          end;
        wdatBinCreated:
          begin
            binID := aBuffer.bb_read_rawbytestring(aCursor);
            TMonitor.Enter(fBins);
            try
              if not fBins.ContainsKey(binID) then
              begin
                bin := TWDBin.Create(Self, binID, True);
                fBins.Add(binID, bin);
              end;
            finally
              TMonitor.Exit(fBins);
            end;
          end;
        wdatBinDeleted:
          begin
            binID := aBuffer.bb_read_rawbytestring(aCursor);
            TMonitor.Enter(fBins);
            try
              fBins.Remove(binID); // todo:
            finally
              TMonitor.Exit(fBins);
            end;
          end;
        // schema
        wdatSchemaSource:
          begin
            sSource := aBuffer.bb_read_string(aCursor);
          end;
        wdatSchemaCollection:
          begin
            sCollectionName := aBuffer.bb_read_string(aCursor);
            // todo: for now we process the schema for all known collections
            // todo: this could change to only registering the schema for the collections known to us..
            sCollection := SchemaCollection(sCollectionName, False);
          end;
        wdatSchemaAttributeName:
          begin
            sAttributeName := aBuffer.bb_read_string(aCursor);
          end;
        wdatSchemaAttributeFunction:
          begin
            sAttributeWorldFunction := aBuffer.bb_read_uint32(aCursor);
          end;
        wdatSchemaAttributeKey:
          begin
            // pre: schemaAttributeName
            // pre: schemaAttributeWorldFunction
            schemaAttributeKey := aBuffer.bb_read_uint32(aCursor);
            if Assigned(sCollection) then
            begin
              TMonitor.Enter(fSchema);
              try
                if sCollection.attributesOnName.TryGetValue(sAttributeName, sAttributeOnName) then
                begin
                  // check the key
                  if schemaAttributeKey<>sAttributeOnName.key then
                  begin
                    // conflict on key: choose which key to use
                    cmp := CompareHighestFunction(sAttributeOnName.worldFunctions, sAttributeWorldFunction);
                    if (cmp<0) or ((cmp=0) and (sAttributeOnName.source<sSource)) then
                    begin // 'they' win, use their key
                      // adjust the specific collection in all bins; schemaAttributeOnName.key -> schemaAttributeKey
                      TMonitor.Enter(fBins);
                      try
                        for bin in fBins.Values do
                        begin
                          if bin.Collections.TryGetValue(sCollectionName, collection) then
                          begin
                            { todo: **** handle in collection
                            if collection.fLocalSchema.attributesOnKey.TryGetValue(schemaAttributeOnName.key, ls) then
                            begin
                              collection.fLocalSchema.attributesOnKey.Remove(schemaAttributeOnName.key);
                              ls.Key := schemaAttributeKey;
                              collection.fLocalSchema.attributesOnKey.Add(ls.Key, ls);
                            end;
                            }
                          end;
                        end;
                      finally
                        TMonitor.Exit(fBins);
                      end;
                      // adjust the general schema
                      sCollection.attributesOnKey.Remove(sAttributeOnName.key);
                      sAttributeOnName.key := schemaAttributeKey;
                      sCollection.attributesOnKey.Add(sAttributeOnName.key, sAttributeOnName);
                    end
                    else
                    begin // we win -> signal our schema registration for this attribute to override on the sender
                      // could be multiple clients sending this: thats ok
                      signalSchemaAttribute(fEvent, sAttributeOnName.source, sCollectionName, sAttributeOnName);
                    end;
                  end;
                  // adjust the world functions registration based on the name
                  sAttributeOnName.worldFunctions := sAttributeOnName.worldFunctions or sAttributeWorldFunction;
                end
                else
                begin
                  if sCollection.attributesOnKey.TryGetValue(schemaAttributeKey, sAttributeOnKey) then
                  begin
                    // conflict on key in schemaAttributeOnName and in schemaAttributeOnKey: must be different
                    // name is not yet known but we have a conflict based on our key, we or they must find a new key
                    cmp := CompareHighestFunction(sAttributeOnKey.worldFunctions, sAttributeWorldFunction);
                    if (cmp<0) or ((cmp=0) and (sAttributeOnKey.source<sSource)) then
                    begin // 'they' win
                      // get new key for existing registration
                      sAttributeOnKey.key := sCollection.NextFreeKey(sAttributeOnKey.key);
                      // adjust the specific collection in all bins; schemaAttributeKey -> schemaAttributeOnKey.key
                      TMonitor.Enter(fBins);
                      try
                        for bin in fBins.Values do
                        begin
                          if bin.Collections.TryGetValue(sCollectionName, collection) then
                          begin
                            { todo: **** handle in collection
                            if collection.fLocalSchema.attributesOnKey.TryGetValue(schemaAttributeKey, ls) then
                            begin
                              collection.fLocalSchema.attributesOnKey.Remove(schemaAttributeKey);
                              ls.Key := schemaAttributeOnKey.key;
                              collection.fLocalSchema.attributesOnKey.Add(ls.Key, ls);
                            end;
                            }
                          end;
                        end;
                      finally
                        TMonitor.Exit(fBins);
                      end;
                      // adjust general schema
                      sCollection.attributesOnKey.Remove(schemaAttributeKey);
                      sCollection.attributesOnKey.Add(sAttributeOnKey.key, sAttributeOnKey);
                      // signal our new general schema
                      signalSchemaAttribute(fEvent, sAttributeOnKey.source, sCollectionName, sAttributeOnKey);
                      // now handle the new received schema registration that won
                      // fill schemaAttributeOnName to add on the schema for this collection
                      sAttributeOnName := TWDSchemaAttribute.Create;
                      sAttributeOnName.name := sAttributeName;
                      sAttributeOnName.worldFunctions := sAttributeWorldFunction;
                      sAttributeOnName.key := schemaAttributeKey;
                      sAttributeOnName.source := sSource;
                      // add and overwrite registration
                      sCollection.attributesOnName.Add(sAttributeOnName.name, sAttributeOnName);
                      sCollection.attributesOnKey.AddOrSetValue(sAttributeOnName.key, sAttributeOnName);
                    end
                    else
                    begin // we win
                      // we reject the senders registration
                      // we send our registration to trigger the overruling of the senders registrationschemaAttributeOnName := TWDSchemaAttribute.Create;
                      signalSchemaAttribute(fEvent, sAttributeOnName.source, sCollectionName, sAttributeOnName);
                    end;
                  end
                  else
                  begin
                    sAttributeOnName := TWDSchemaAttribute.Create;
                    sAttributeOnName.key := schemaAttributeKey;
                    sAttributeOnName.name := sAttributeName;
                    sAttributeOnName.worldFunctions := sAttributeWorldFunction;
                    sAttributeOnName.source := sSource;
                    // add and overwrite registration
                    sCollection.attributesOnName.Add(sAttributeOnName.name, sAttributeOnName);
                    sCollection.attributesOnKey.AddOrSetValue(sAttributeOnName.key, sAttributeOnName);
                  end;
                end;
              finally
                TMonitor.Exit(fSchema);
              end;
            end
            else Log.WriteLn('Received schema attribute without schema name: '+schemaAttributeKey.ToString+'('+sAttributeName+', '+sCollectionName+')');
          end;
      else
        aBuffer.bb_read_skip(aCursor, key and 7); // 7306=913=bin descr 7322=915=bin ref 7328=916=bin state 7314=914=bin parent
      end;
    end;
  except
    on E: Exception
    do Log.WriteLn('Exception in TWDDataSources.handleDataSourcesEvent: '+E.Message, llError);
  end;
end;

procedure TWDDataSources.handleResponse(const aResponse: TByteBuffer);
begin
  handleDataSourcesEvent(fEvent, aResponse, 0, length(aResponse));
end;

function TWDDataSources.SchemaCollection(const aName: string; aOnlyExisting: Boolean): TWDSchemaCollection;
begin
  TMonitor.Enter(fSchema);
  try
    if not fSchema.TryGetValue(aName, Result) then
    begin
      if not aOnlyExisting then
      begin
        Result := TWDSchemaCollection.Create;
        fSchema.Add(aName, Result);
      end
      else Result := Nil;
    end;
  finally
    TMonitor.Exit(fSchema);
  end;
end;

procedure TWDDataSources.signalBinCopy(aEventEntry: TEventEntry; const aSrcID, aDstID: TWDID);
begin
  aEventEntry.signalEvent(
    TByteBuffer.bb_tag_rawbytestring(wdatBinSourceID shr 3, aSrcID)+
    TByteBuffer.bb_tag_rawbytestring(wdatBinCopy shr 3, aDstID));
end;

procedure TWDDataSources.signalBinCreate(aEventEntry: TEventEntry; const aBinID: TWDID; const aDescription: string; aParentID, aRefID: TWDID);
begin
  aEventEntry.signalEvent(
    TByteBuffer.bb_tag_string(wdatBinDescription shr 3, aDescription)+
    TByteBuffer.bb_tag_rawbytestring(wdatBinParent shr 3, aParentID)+
    TByteBuffer.bb_tag_rawbytestring(wdatBinRef shr 3, aRefID)+
    TByteBuffer.bb_tag_rawbytestring(wdatBinCreate shr 3, aBinID));
end;

procedure TWDDataSources.signalBinDelete(aEventEntry: TEventEntry; const aBinID: TWDID);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_rawbytestring(wdatBinDelete shr 3, aBinID));
end;

procedure TWDDataSources.signalInquireBins(aEventEntry: TEventEntry; const aReturnEventName: string);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_string(wdatBinsInquire shr 3, aReturnEventName));
end;

procedure TWDDataSources.signalInquireDataSources(aEventEntry: TEventEntry; const aReturnEventName: string);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_string(wdatDataSourcesInquire shr 3, aReturnEventName));
end;

procedure TWDDataSources.signalInquireSchema(aEventEntry: TEventEntry; const aReturnEventName: string);
begin
  aEventEntry.signalEvent(TByteBuffer.bb_tag_string(wdatSchemaInquire shr 3, aReturnEventName));
end;

procedure TWDDataSources.signalSchemaAttribute(aEventEntry: TEventEntry; const aSchemaSource, aSchemaCollectionName: string;
  const aSchemaAttribute: TWDSchemaAttribute);
begin
  aEventEntry.signalEvent(
    TByteBuffer.bb_tag_string(wdatSchemaSource shr 3, aSchemaSource{fPrivateEvent.eventName})+
    TByteBuffer.bb_tag_string(wdatSchemaCollection shr 3, aSchemaCollectionName)+
    TByteBuffer.bb_tag_string(wdatSchemaAttributeName shr 3, aSchemaAttribute.name)+
    TByteBuffer.bb_tag_uint32(wdatSchemaAttributeFunction shr 3, aSchemaAttribute.worldFunctions)+
    TByteBuffer.bb_tag_uint32(wdatSchemaAttributeKey shr 3, aSchemaAttribute.key));
end;

procedure TWDDataSources.signalSchemaAttribute(aEventEntry: TEventEntry; const aSchemaSource, aSchemaCollectionName: string;
  const aSchemaAttribute: TWDClassCacheField);
begin
  aEventEntry.signalEvent(
    TByteBuffer.bb_tag_string(wdatSchemaSource shr 3, aSchemaSource{fPrivateEvent.eventName})+
    TByteBuffer.bb_tag_string(wdatSchemaCollection shr 3, aSchemaCollectionName)+
    TByteBuffer.bb_tag_string(wdatSchemaAttributeName shr 3, aSchemaAttribute.name)+
    TByteBuffer.bb_tag_uint32(wdatSchemaAttributeFunction shr 3, aSchemaAttribute.Functions)+
    TByteBuffer.bb_tag_uint32(wdatSchemaAttributeKey shr 3, aSchemaAttribute.key));
end;

{ TWDAttrProcDouble }

constructor TWDAttrProcDouble<T>.Create(const aProcessor: TWDAttrProcDoubleProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcDouble<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_double(aCursor));
end;

{ TWDAttrProcSingle }

constructor TWDAttrProcSingle<T>.Create(const aProcessor: TWDAttrProcSingleProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcSingle<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_single(aCursor));
end;

{ TWDAttrProcString }

constructor TWDAttrProcString<T>.Create(const aProcessor: TWDAttrProcStringProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcString<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_string(aCursor));
end;

{ TWDAttrProcTByteBuffer }

constructor TWDAttrProcTByteBuffer<T>.Create(const aProcessor: TWDAttrProcTByteBufferProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcTByteBuffer<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_rawbytestring(aCursor));
end;

{ TWDAttrProcBoolean }

constructor TWDAttrProcBoolean<T>.Create(const aProcessor: TWDAttrProcBooleanProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcBoolean<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_bool(aCursor));
end;

{ TWDAttrProcUInt32 }

constructor TWDAttrProcUInt32<T>.Create(const aProcessor: TWDAttrProcUInt32Processor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcUInt32<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_uint32(aCursor));
end;

{ TWDAttrProcInt32 }

constructor TWDAttrProcInt32<T>.Create(const aProcessor: TWDAttrProcInt32Processor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcInt32<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_int32(aCursor));
end;

{ TWDAttrProcUInt64 }

constructor TWDAttrProcUInt64<T>.Create(const aProcessor: TWDAttrProcUInt64Processor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcUInt64<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_uint64(aCursor));
end;

{ TWDAttrProcInt64 }

constructor TWDAttrProcInt64<T>.Create(const aProcessor: TWDAttrProcInt64Processor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcInt64<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer.bb_read_int64(aCursor));
end;

{ TWDAttrProcClass }

constructor TWDAttrProcClass<T>.Create(const aProcessor: TWDAttrProcClassProcessor<T>);
begin
  Inherited Create;
  fProcessor := aProcessor;
end;

procedure TWDAttrProcClass<T>.ProcessAttribute(aCollection: TWDCollection<T>; const aID: TWDID; var aObject: T; aTag: Integer; const aBuffer: TByteBuffer; var aCursor: Integer; aLimit: Integer);
begin
  fProcessor(aCollection, aID, aObject, aTag, aBuffer, aCursor, aLimit);
end;

{ TWDCObjectUpdate }

procedure TWDCObjectUpdate.Commit;
begin
  TMonitor.Enter(Self);
  try
    CommitL;
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TWDCObjectUpdate.CommitL;
begin
  if fBuffer<>'' then
  begin
    fEventEntry.signalEvent(TByteBuffer.bb_tag_rawbytestring(wdatObjectID shr 3, fLastID)+fBuffer);
    fBuffer := '';
  end;
  fLastTimeStamp := NaN;
end;

constructor TWDCObjectUpdate.Create(aEventEntry: TEventEntry; aMaxPacketSize: Integer);
begin
  inherited Create;
  fEventEntry := aEventEntry;
  fMaxPacketSize := aMaxPacketSize;
  fBuffer := '';
  fLastID := '';
end;

destructor TWDCObjectUpdate.Destroy;
begin
  Commit;
  inherited;
end;

procedure TWDCObjectUpdate.setID(const aID: TWDID);
begin
  TMonitor.Enter(Self);
  try
    CommitL;
    fLastID := aID;
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TWDCObjectUpdate.setValueRaw(const aValue: RawByteString; aTimeStamp: Double);
begin
  TMonitor.Enter(Self);
  try
    if length(fBuffer)+length(aValue)>fMaxPacketSize
    then CommitL;
    if aTimeStamp.IsNan then
    begin
      if not fLastTimeStamp.IsNan
      then CommitL; // start again with fresh (unset) timestamp
    end
    else
    begin
      // pre: aTimeStamp is not NaN
      if fLastTimeStamp.IsNan or (aTimeStamp<>fLastTimeStamp) then
      begin
        // todo: there was no room checking in the commit check above that takes into account
        // the bytes needed for the time stamp.. could be over limit a few bytes..
        fBuffer := fBuffer+TByteBuffer.bb_tag_double(wdatTimeStamp shr 3, aTimeStamp);
        fLastTimeStamp := aTimeStamp;
      end;
    end;
    fBuffer := fBuffer+aValue;
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: Int64; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_int64(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: Int32; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_int32(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: Double; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_double(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: Single; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_single(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; const aValue: RawByteString; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_rawbytestring(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; const aValue: string; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_string(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: UInt64; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_uint64(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: UInt32; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_uint32(aTag, aValue), aTimeStamp);
end;

procedure TWDCObjectUpdate.setValue(aTag: UInt32; aValue: TWDClass; aTimeStamp: Double);
begin
  setValueRaw(TByteBuffer.bb_tag_rawbytestring(aTag, aValue.Encode), aTimeStamp);
end;

end.
