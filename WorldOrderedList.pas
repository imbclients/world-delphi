unit WorldOrderedList;

interface

uses
  System.Generics.Collections,
  System.Generics.Defaults,
  System.Classes;

type
  TOrderedListTSEntry = class
  constructor Create(aTimeStamp: TDateTime);
  protected
    fTimeStamp: TDateTime;
  public
    property timeStamp: TDateTime read fTimeStamp;
  end;

  TOrderedListTS<T: TOrderedListTSEntry> = class(TObjectList<T>)
  constructor Create();
  protected
    function IndexOfTS(aTimeStamp: TDateTime): Integer;
    function getItemsTS(aTimeStamp: TDateTime; aExact: Boolean=False): T;
  public
    procedure AddTS(aEntry: TOrderedListTSEntry);
    function RemoveTS(aTimeStamp: TDateTime): Boolean;
    function isOrdered: Boolean;
    property ItemsTS[aTimeStamp: TDateTime; aExact: Boolean=False]: T read getItemsTS;
  end;

implementation

{ TOrderedListTSEntry }

constructor TOrderedListTSEntry.Create(aTimeStamp: TDateTime);
begin
  inherited Create;
  fTimeStamp := aTimeStamp;
end;

{ TOrderedListTS }

procedure TOrderedListTS<T>.AddTS(aEntry: TOrderedListTSEntry);
var
  i: Integer;
begin
  // todo: maybe enough: Insert(IndexOfTS(aEntry.timeStamp)+1, aEntry);
  if (Count>0) and (Items[Count-1].timeStamp>aEntry.timeStamp) then
  begin
    i := IndexOfTS(aEntry.timeStamp);
    Insert(i+1, aEntry);
  end
  else Add(aEntry);
end;

constructor TOrderedListTS<T>.Create;
begin
  inherited Create(TComparer<T>.Construct(
    // order slices on date/time
    function(const Left, Right: T): Integer
    begin
      if Left.timeStamp < Right.timeStamp then
        Result := -1
      else if Left.timeStamp > Right.timeStamp then
        Result := 1
      else
        Result := 0;
    end));
end;

function TOrderedListTS<T>.getItemsTS(aTimeStamp: TDateTime; aExact: Boolean): T;
var
  i: Integer;
  item: TOrderedListTSEntry;
begin
  i := IndexOfTS(aTimeStamp);
  if i>=0 then
  begin
    item := Items[i];
    if (not aExact) or (item.timeStamp=aTimeStamp)
    then Result := item as T
    else Result := nil;
  end
  else Result := nil;
end;

// returns highest item with timestamp lower or equal then aTimeStamp
function TOrderedListTS<T>.IndexOfTS(aTimeStamp: TDateTime): Integer;
begin
  Result := Count-1;
  while (Result>=0) and (Items[Result].timeStamp>aTimeStamp)
  do Result := Result-1;
end;

function TOrderedListTS<T>.isOrdered: Boolean;
var
  i: Integer;
begin
  for i := 0 to Count-2 do
  begin
    if Items[i].timeStamp>Items[i+1].timeStamp
    then exit(False);
  end;
  Exit(True);
end;

function TOrderedListTS<T>.RemoveTS(aTimeStamp: TDateTime): Boolean;
var
  i: Integer;
begin
  i := IndexOfTS(aTimeStamp);
  if i>=0 then
  begin
    if Items[i].timeStamp=aTimeStamp then
    begin
      Delete(i);
      Result := True;
    end
    else Result := False;
  end
  else Result := False;
end;

end.
