program TestOrderedList;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  WorldOrderedList,
  System.SysUtils;

procedure Test;
var
  list: TOrderedListTS;
begin
  list := TOrderedListTS.Create;
  try
    list.AddTS(TOrderedListTSEntry.Create(Now));
    Sleep(100);
    list.AddTS(TOrderedListTSEntry.Create(Now));
    Sleep(100);
    list.AddTS(TOrderedListTSEntry.Create(Now));


    list.AddTS(TOrderedListTSEntry.Create(Now-5));
    list.AddTS(TOrderedListTSEntry.Create(Now-3));
    list.AddTS(TOrderedListTSEntry.Create(Now-8));
    list.AddTS(TOrderedListTSEntry.Create(Now+1));
    list.AddTS(TOrderedListTSEntry.Create(Now+3));
    list.AddTS(TOrderedListTSEntry.Create(Now+2));


    WriteLn('Ordered: ', list.isOrdered);

    list.Sort;

    WriteLn('Ordered: ', list.isOrdered);

    ReadLn;
  finally
    list.Free;
  end;
end;

begin
  try
    Test;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
