unit WorldJSON;

{
  this unit is meant to create json from code easier

    with WDJSONCreate(o) do
    begin
      add('integerValuePair', 1);
      add('doubleValuePair', 5.4);
      add('doubleThatIsNaN', Double.NaN);
      add('stringValuePair', 'text');
      add('booleanValuePair', true);
      add('nullValuePair');
      with addObject('objectInObject') do
      begin
        add('secondIntegerValue', 2);
      end;
      with addArray('arrayValuePair') do
      begin
        add(2);
        add(3.4);
        add(true);
        add('string');
        with addArray() do // add array within array
        begin
          add(41);
          add(42);
        end;
        with addObject() do // object within array
        begin
          add('integerValuePair', 5);
          add('booleanValuePair', True);
        end;
      end;
    end;
    WriteLn(o.JSON(pretty));
}

interface

uses
  // world
  WorldDataCode,
  // delphi standard
  System.JSON,
  System.generics.collections,
  System.SysUtils;

const
  notPretty = -1; // use as a parameter to get compressed non-pretty formatted json returned from JSON function
  pretty = 0; // use as a parameter to get pretty formatted json returned from JSON function

  ccBackspace = #08;
  ccFormFeed = #12;
  ccNewLine = #10;
  ccCarriageReturn = #13;
  ccHorizontalTab = #9;
  ccCRLF = ccCarriageReturn+ccNewLine;


type
  TWDJSONValue = class
  protected
    function getJSON(aPretty: Integer): string; virtual;
    function jsonEscape(const aJSONString: string): string;
    function jsonUnEscape(const aJSONString: string): string;
    function indent(aPretty: Integer; aCRLF: Boolean=True): string;
  public
    function JSON(aPretty: Integer=notPretty): string;
  end;

  TWDJSONStringValue = class(TWDJSONValue)
  constructor Create(const aValue: string);
  private
    fValue: string;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: string read fValue write fValue;
  end;

  TWDJSONDoubleValue = class(TWDJSONValue)
  constructor Create(aValue: Double);
  private
    fValue: Double;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: Double read fValue write fValue;
  end;

  TWDJSONIntegerValue = class(TWDJSONValue)
  constructor Create(aValue: Integer);
  private
    fValue: Integer;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: Integer read fValue write fValue;
  end;

  TWDJSONBooleanValue = class(TWDJSONValue)
  constructor Create(aValue: Boolean);
  private
    fValue: Boolean;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: Boolean read fValue write fValue;
  end;

  TWDJSONTJSONObjectAsValue = class(TWDJSONValue)
  constructor Create(aValue: TJSONObject; aOwnsValue: Boolean);
  destructor Destroy; override;
  private
    fValue: TJSONObject;
    fOwnsValue: Boolean;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: TJSONObject read fValue; // write fValue;
  end;

  TWDJSONObjectAsText = class(TWDJSONValue)
  constructor Create(const aValue: string);
  private
    fValue: string;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property value: string read fValue write fValue;
  end;

  TWDJSONObject = class; // forward

  TWDJSONArray = class(TWDJSONValue)
  constructor Create;
  destructor Destroy; override;
  private
    fValues: TObjectList<TWDJSONValue>;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property values: TObjectList<TWDJSONValue> read fValues;
  public
    function add(const aValue: string): TWDJSONStringValue; overload;
    function add(aValue: Double): TWDJSONDoubleValue; overload;
    function add(aValue: Integer): TWDJSONIntegerValue; overload;
    function add(aValue: Boolean): TWDJSONBooleanValue; overload;
    function addArray(): TWDJSONArray;
    function addObject(): TWDJSONObject;
  end;

  TWDJSONObject = class(TWDJSONValue)
  constructor Create;
  destructor Destroy; override;
  private
    fValues: TObjectDictionary<string, TWDJSONValue>;
  protected
    function getJSON(aPretty: Integer): string; override;
  public
    property values: TObjectDictionary<string, TWDJSONValue> read fValues;
  public
    procedure add(const aName: string); overload;
    function add(const aName: string; const aValue: string): TWDJSONStringValue; overload;
    function add(const aName: string; aValue: Double): TWDJSONDoubleValue; overload;
    function add(const aName: string; aValue: Integer): TWDJSONIntegerValue; overload;
    function add(const aName: string; aValue: Boolean): TWDJSONBooleanValue; overload;
    function add(const aName: string; aValue: TWDJSONValue): TWDJSONValue; overload;
    function add(const aName: string; aValue: TJSONObject; aOwnsValue: Boolean): TWDJSONTJSONObjectAsValue; overload;
    function addArray(const aName: string): TWDJSONArray;
    function addObject(const aName: string): TWDJSONObject;
  end;

function WDJSONCreate(out aObject: TWDJSONObject): TWDJSONObject;
function WDJSONParse(const aJSON:string; out aObject: TWDJSONObject): TWDJSONObject;


implementation

function WDJSONCreate(out aObject: TWDJSONObject): TWDJSONObject;
begin
  aObject := TWDJSONObject.Create;
  Result := aObject;
end;

procedure WDJSONCopyObject(aWDObject: TWDJSONObject; aJSONObject: TJSONObject); forward;
procedure WDJSONCopyArray(aWDArray: TWDJSONArray; aJSONArray: TJSONArray); forward;

function WDJSONCreateValue(aJSONValue: TJSONValue): TWDJSONValue;
begin
  if aJSONValue is TJSONNull
  then Result := TWDJSONValue.Create // null value
  else if aJSONValue is TJSONNumber
  then Result := TWDJSONDoubleValue.Create((aJSONValue as TJSONNumber).AsDouble)
  else if aJSONValue is TJSONString
  then Result := TWDJSONStringValue.Create((aJSONValue as TJSONString).Value)
  else if aJSONValue is TJSONBool
  then Result := TWDJSONBooleanValue.Create((aJSONValue as TJSONBool).AsBoolean)
  else if aJSONValue is TJSONObject then
  begin
    Result := TWDJSONObject.Create;
    WDJSONCopyObject(Result as TWDJSONObject, aJSONValue as TJSONObject);
  end
  else if aJSONValue is TJSONArray then
  begin
    Result := TWDJSONArray.Create;
    WDJSONCopyArray(Result as TWDJSONArray, aJSONValue as TJSONArray);
  end
  else raise Exception.Create('Unsupported JSON type in object parsing: '+aJSONValue.ClassName);
end;

procedure WDJSONCopyArray(aWDArray: TWDJSONArray; aJSONArray: TJSONArray);
var
  i: Integer;
begin
  for i := 0 to aJSONArray.Count-1
  do aWDArray.values.Add(WDJSONCreateValue(aJSONArray.Items[i]));
end;

procedure WDJSONCopyObject(aWDObject: TWDJSONObject; aJSONObject: TJSONObject);
var
  i: Integer;
begin
  for i := 0 to aJSONObject.Count-1
  do aWDObject.values.AddOrSetValue(aJSONObject.Pairs[i].JsonString.Value, WDJSONCreateValue(aJSONObject.Pairs[i].JsonValue));
end;

function WDJSONParse(const aJSON:string; out aObject: TWDJSONObject): TWDJSONObject;
var
  jsonObject: TJSONObject;
begin
  Result := TWDJSONObject.Create;
  // todo: parse by using standard Delphi JSON support and convert to simpler structure defined in this unit
  jsonObject := TJSONObject.ParseJSONValue(aJSON) as TJSONObject;
  try
    WDJSONCopyObject(Result, jsonObject);
  finally
    jsonObject.Free;
  end;
end;

{ TWDJSONValue }

function TWDJSONValue.getJSON(aPretty: Integer): string;
begin
  Result := 'null';
end;

function TWDJSONValue.indent(aPretty: Integer; aCRLF: Boolean): string;
begin
  if aPretty>=0 then
  begin
    if aCRLF
    then Result := ccCRLF
    else Result := '';
    while aPretty>0 do
    begin
      Result := Result+ccHorizontalTab;
      aPretty := aPretty-1;
    end;
  end
  else Result := '';
end;

function TWDJSONValue.JSON(aPretty: Integer): string;
begin
  Result := getJSON(aPretty);
end;

function TWDJSONValue.jsonEscape(const aJSONString: string): string;
var
  c: Char;
begin
  Result := '';
  for c in aJSONString do
  begin
    case c of
      '"':
        Result := Result+'\"';
      '\':
        Result := Result+'\\';
      '/':
        Result := Result+'\/';
      ccBackspace:
        Result := Result+'\b';
      ccFormFeed:
        Result := Result+'\f';
      ccNewLine:
        Result := Result+'\n';
      ccCarriageReturn:
        Result := Result+'\r';
      ccHorizontalTab:
        Result := Result+'\t';
    else
      Result := Result+c;
    end;
  end;
end;

function TWDJSONValue.jsonUnEscape(const aJSONString: string): string;
var
  i: Integer;
begin
  Result := '';
  i := 0;
  while i<length(aJSONString) do
  begin
    if aJSONString[i+1]='\' then
    begin
      case aJSONString[i+2] of
        '"', '\', '/':
          Result := Result+aJSONString[i+2];
        'b':
          Result := Result+ccBackspace;
        'f':
          Result := Result+ccFormFeed;
        'n':
          Result := Result+ccNewLine;
        'r':
          Result := Result+ccCarriageReturn;
        't':
          Result := Result+ccHorizontalTab;
      else
        raise Exception.Create('Invalid escape char "'+aJSONString.Substring(i, 2)+'" @ '+i.ToString+' in JSON string');
      end;
      // skip extra code char
      i := i+1;
    end
    else Result := Result+aJSONString[i+1];
    i := i+1;
  end;
end;

{ TWDJSONStringValue }

constructor TWDJSONStringValue.Create(const aValue: string);
begin
  inherited Create;
  fValue := aValue;
end;

function TWDJSONStringValue.getJSON(aPretty: Integer): string;
begin
  Result := '"'+jsonEscape(fValue)+'"';
end;

{ TWDJSONDoubleValue }

constructor TWDJSONDoubleValue.Create(aValue: Double);
begin
  inherited Create;
  fValue := aValue;
end;

function TWDJSONDoubleValue.getJSON(aPretty: Integer): string;
begin
  if not value.IsNan
  then Result := fValue.toString(dotFormat)
  else Result := inherited getJSON(aPretty);
end;

{ TWDJSONIntegerValue }

constructor TWDJSONIntegerValue.Create(aValue: Integer);
begin
  inherited Create;
  fValue := aValue;
end;

function TWDJSONIntegerValue.getJSON(aPretty: Integer): string;
begin
  Result := fValue.toString();
end;

{ TWDJSONBooleanValue }

constructor TWDJSONBooleanValue.Create(aValue: Boolean);
begin
  inherited Create;
  fValue := aValue;
end;

function TWDJSONBooleanValue.getJSON(aPretty: Integer): string;
begin
  if fValue
  then Result := 'true'
  else Result := 'false';
end;

{ TWDJSONTJSONObjectAsValue }

constructor TWDJSONTJSONObjectAsValue.Create(aValue: TJSONObject; aOwnsValue: Boolean);
begin
  inherited Create;
  fValue := aValue;
  fOwnsValue := aOwnsValue;
end;

destructor TWDJSONTJSONObjectAsValue.Destroy;
begin
  if fOwnsValue
  then fValue.Free;
  fValue := nil;
  inherited;
end;

function TWDJSONTJSONObjectAsValue.getJSON(aPretty: Integer): string;
begin
  Result := fValue.ToJSON;
end;

{ TWDJSONObjectAsString }

constructor TWDJSONObjectAsText.Create(const aValue: string);
begin
  inherited Create;
  fValue := aValue;
end;

function TWDJSONObjectAsText.getJSON(aPretty: Integer): string;
begin
  if fValue<>''
  then Result := fValue
  else Result := inherited;
end;

{ TWDJSONArray }

//function TWDJSONArray.add(aValue: TWDJSONValue): TWDJSONValue;
//begin
//  values.Add(aValue);
//  Result := aValue;
//end;

function TWDJSONArray.add(aValue: Double): TWDJSONDoubleValue;
begin
  Result := TWDJSONDoubleValue.Create(aValue);
  values.Add(Result);
end;

function TWDJSONArray.add(const aValue: string): TWDJSONStringValue;
begin
  Result := TWDJSONStringValue.Create(aValue);
  values.Add(Result);
end;

function TWDJSONArray.add(aValue: Boolean): TWDJSONBooleanValue;
begin
  Result := TWDJSONBooleanValue.Create(aValue);
  values.Add(Result);
end;

function TWDJSONArray.add(aValue: Integer): TWDJSONIntegerValue;
begin
  Result := TWDJSONIntegerValue.Create(aValue);
  values.Add(Result);
end;

constructor TWDJSONArray.Create;
begin
  inherited Create;
  fValues := TObjectList<TWDJSONValue>.Create(True);
end;

destructor TWDJSONArray.Destroy;
begin
  FreeAndNil(fValues);
  inherited;
end;

function TWDJSONArray.getJSON(aPretty: Integer): string;
var
  v: TWDJSONValue;
begin
  Result := '';
  try
    if aPretty>=0
    then aPretty := aPretty+1;
    try
      for v in fValues do
      begin
        if Result<>''
        then Result := Result+',';
        Result := Result+indent(aPretty, not((v is TWDJSONArray) or (v is TWDJSONObject)))+v.getJSON(aPretty);
      end;
    finally
      if aPretty>=0
      then aPretty := aPretty-1;
    end;
  finally
    Result := indent(aPretty)+'['+Result+indent(aPretty)+']';
  end;
end;

function TWDJSONArray.addArray: TWDJSONArray;
begin
  Result := TWDJSONArray.Create();
  values.Add(Result);
end;

function TWDJSONArray.addObject: TWDJSONObject;
begin
  Result := TWDJSONObject.Create();
  values.Add(Result);
end;

{ TWDJSONObject }

function TWDJSONObject.addArray(const aName: string): TWDJSONArray;
begin
  Result := TWDJSONArray.Create;
  values.AddOrSetValue(aName, Result);
end;

function TWDJSONObject.add(const aName, aValue: string): TWDJSONStringValue;
begin
  Result := TWDJSONStringValue.Create(aValue);
  values.AddOrSetValue(aName, Result);
end;

function TWDJSONObject.add(const aName: string; aValue: Double): TWDJSONDoubleValue;
begin
  Result := TWDJSONDoubleValue.Create(aValue);
  values.AddOrSetValue(aName, Result);
end;

function TWDJSONObject.add(const aName: string; aValue: Integer): TWDJSONIntegerValue;
begin
  Result := TWDJSONIntegerValue.Create(aValue);
  values.AddOrSetValue(aName, Result);
end;

function TWDJSONObject.add(const aName: string; aValue: Boolean): TWDJSONBooleanValue;
begin
  Result := TWDJSONBooleanValue.Create(aValue);
  values.AddOrSetValue(aName, Result);
end;

procedure TWDJSONObject.add(const aName: string);
begin
  values.AddOrSetValue(aName, TWDJSONValue.Create); // null value
end;

function TWDJSONObject.add(const aName: string; aValue: TJSONObject; aOwnsValue: Boolean): TWDJSONTJSONObjectAsValue;
begin
  Result := TWDJSONTJSONObjectAsValue.Create(aValue, aOwnsValue);
  values.AddOrSetValue(aName, Result);
end;

function TWDJSONObject.add(const aName: string; aValue: TWDJSONValue): TWDJSONValue;
begin
  Result := aValue;
  values.AddOrSetValue(aName, Result);
end;

constructor TWDJSONObject.Create;
begin
  inherited Create;
  fValues := TObjectDictionary<string, TWDJSONValue>.Create([doOwnsValues]);
end;

destructor TWDJSONObject.Destroy;
begin
  FreeAndNil(fValues);
  inherited;
end;

function TWDJSONObject.getJSON(aPretty: Integer): string;
var
  nvp: TPair<string, TWDJSONValue>;
begin
  Result := '';
  try
    if aPretty>=0
    then aPretty := aPretty+1;
    try
      for nvp in fValues do
      begin
        if Result<>''
        then Result := Result+',';
        Result := Result+indent(aPretty)+'"'+jsonEscape(nvp.Key)+'":'+nvp.Value.getJSON(aPretty);
      end;
    finally
      if aPretty>=0
      then aPretty := aPretty-1;
    end;
  finally
    Result := indent(aPretty)+'{'+Result+indent(aPretty)+'}';
  end;
end;

function TWDJSONObject.addObject(const aName: string): TWDJSONObject;
begin
  Result := TWDJSONObject.Create;
  values.AddOrSetValue(aName, Result);
end;

end.
